
apt -y install libboost-all-dev
apt -y install ninja-build
apt -y install libgtest-dev

wget https://bootstrap.pypa.io/get-pip.py
python3 get-pip.py
pip3 install numpy

wget ftp://ftp.funet.fi/pub/gnu/prep/gcc/gcc-7.3.0/gcc-7.3.0.tar.xz
tar xf gcc-7.3.0.tar.xz
cd gcc-7.3.0

#wget ftp://ftp.funet.fi/pub/gnu/prep/gcc/gcc-8.1.0/gcc-8.1.0.tar.xz
#tar xf gcc-8.1.0.tar.xz
#cd gcc-8.1.0
./contrib/download_prerequisites
mkdir build
cd build
#../configure -v --build=x86_64-linux-gnu --host=x86_64-linux-gnu --target=x86_64-linux-gnu --prefix=/usr/local/gcc-8.1 --enable-checking=release --enable-languages=c,c++,fortran --disable-multilib --program-suffix=-8.1
../configure -v --build=x86_64-linux-gnu --host=x86_64-linux-gnu --target=x86_64-linux-gnu --prefix=/usr/local/gcc-7.3 --enable-checking=release --enable-languages=c,c++,fortran --disable-multilib --program-suffix=-7.3
make -j4
make install

#export CC=/usr/local/gcc-8.1/bin/gcc-8.1
#export CXX=/usr/local/gcc-8.1/bin/g++-8.1
export CC=/usr/local/gcc-7.3/bin/gcc-7.3
export CXX=/usr/local/gcc-7.3/bin/g++-7.3


version=3.11
build=1
wget https://cmake.org/files/v$version/cmake-$version.$build.tar.gz
tar -xzvf cmake-$version.$build.tar.gz
cd cmake-$version.$build/
./bootstrap --prefix=/usr
make -j12
make install

mkdir data
cd data

wget https://snap.stanford.edu/data/CollegeMsg.txt.gz
gzip -d CollegeMsg.txt.gz

wget https://doi.org/10.1371/journal.pcbi.1001109.s001
mv journal.pcbi.1001109.s001 journal.pcbi.1001109.s001.csv

wget http://www.sociopatterns.org/files/datasets/003/ht09_contact_list.dat.gz
gzip -d ht09_contact_list.dat.gz

cd ..

mkdir deps
cd deps
mkdir install

git clone https://github.com/eigenteam/eigen-git-mirror.git eigen
cd eigen
git checkout d58f73408e2b31dcb4244001df551b71f2aa96b6
mkdir build
cd build
cmake .. -G Ninja -DCMAKE_BUILD_TYPE=Release
cmake --build . --target install
cd ../..

git clone  https://github.com/gflags/gflags.git gflags
cd gflags
git checkout e292e0452fcfd5a8ae055b59052fc041cbab4abf
mkdir build
cd build
cmake .. -G Ninja -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../../install
cmake --build . --target install
cd ../..


git clone https://github.com/Microsoft/GSL.git gsl
cd gsl
git checkout c87c123d1b3e64ae2cf725584f0c004da4d90f1c
mkdir build
cd build
cmake .. -G Ninja -DCMAKE_BUILD_TYPE=Release -DGSL_TEST=Off
cmake --build . --target install
cd ../..

git clone https://github.com/ericniebler/range-v3.git range-v3
cd range-v3
git checkout 2ff4cf2a23b15eaa36dc347c30d6eac0f9608c58
mkdir build
cd build
cmake .. -G Ninja -DCMAKE_BUILD_TYPE=Release -DRANGE_V3_NO_TESTING=On
cmake --build . --target install
cd ../..


git clone https://github.com/fmtlib/fmt fmt
cd fmt
git checkout 867b330966f7082d196852f3a7f68c8d1cba83f0 
mkdir build
cd build
cmake .. -G Ninja  -DCMAKE_BUILD_TYPE=Release
cmake --build . --target install
cd ../..

git clone https://github.com/google/googletest.git googletest
cd googletest
git checkout 1.8.0 
mkdir build
cd build
cmake .. -G Ninja  -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=../../install
cmake --build . --target install
cd ../..

cd ..

git clone https://bitbucket.org/xyzkwadrat/code.git
mkdir build
cd build
cmake .. -DCMAKE_PREFIX_PATH=../deps/install/lib/ ../code/src -DCMAKE_BUILD_TYPE=Release
make -j12

cd ..
python3 code/tools/runner.py --help
