mkdir deps
cd deps
git clone https://github.com/eigenteam/eigen-git-mirror.git eigen
git clone https://github.com/google/googletest.git

exit
cmake .. -G "Visual Studio 15 2017 Win64"
# cmake --build . --target INSTALL --config Release --clean-first 

cmake ..\src -G "Visual Studio 15 2017 Win64" -DGTEST_INCLUDE_DIR="C:\Program Files\googletest-distribution\include" -DGTEST_LIBRARY="C:\Program Files\googletest-distribution\lib\gtest.lib" -DGTEST_MAIN_LIBRARY="C:\Program Files\googletest-distribution\lib\gtest_main.lib" -Dfmt_DIR

wget https://dl.bintray.com/boostorg/release/1.66.0/source/boost_1_66_0.7z

cmake ..\src -G "Visual Studio 15 2017 Win64" -DGTEST_INCLUDE_DIR="C:\Program Files\googletest-distribution\include" -DGTEST_LIBRARY="C:\Program Files\goog
letest-distribution\lib\gtest.lib" -DGTEST_MAIN_LIBRARY="C:\Program Files\googletest-distribution\lib\gtest_main.lib" -DBOOST_ROOT="C:\boost_1_66_0" -Dfmt_DIR="..\deps\fmt\build"

cmake .. -G "Visual Studio 15 2017 Win64" -DCMAKE_INSTALL_PREFIX=..\install -Tv141_clang_c2