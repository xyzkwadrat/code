import os
import pandas
import numpy


def read_header(filepath):
    headermap = {}
    with open(filepath) as file:
        for line in file:
            if line.startswith('#') and ':' in line:
                key, value = line[1:].strip().split(':')
                headermap[key] = value
            else:
                break
    return headermap


def get_data(filepath):
    try:
        header = read_header(filepath)
        required_lenght = int(header['matrix_count'])
        df = pandas.read_csv(
            filepath,
            delimiter=';',
            comment='#',
            dtype=numpy.float,
            names=['meanPrevalence', 'meanIncidence'])
        if required_lenght != df.shape[0]:
            padding = pandas.DataFrame(
                numpy.zeros((required_lenght - df.shape[0], df.shape[1])), columns=df.columns)
            df = pandas.concat([df, padding], axis=0, ignore_index=True)

        df['cumulative incidence'] = df.meanIncidence.cumsum() + df.meanPrevalence[0]
    except Exception as e:
        print(f"Failed to read data from {filepath} with error {e}")
        raise
    return df


def extension(filename):
    return os.path.splitext(filename)[-1]


CACHE_NAME = "__cache__"


def cache_path(directory) -> str:
    (root, dirname) = os.path.split(directory)
    return os.path.join(root, CACHE_NAME, dirname + '.h5')


def is_cache(path):
    dirname = os.path.split(path)[-1]
    return dirname == CACHE_NAME


def initialize_cache_dir_if_missing(directory):
    (path, _) = os.path.split(cache_path(directory))
    os.makedirs(path, exist_ok=True)


def get_cached_data(directory):
    cache = cache_path(directory)
    if os.path.isfile(cache):
        with pandas.HDFStore(cache) as store:
            return store['data']
    else:
        raise IOError("Missing file {}".format(cache))


def generate_cache(directory, data):
    initialize_cache_dir_if_missing(directory)
    cache = cache_path(directory)
    try:
        with pandas.HDFStore(cache) as store:
            store.put('data', data)
    except:
        os.remove(cache)
        raise


def get_data_from_directory(directory, required_files=None, disable_cache=False):
    all_data = []
    filenames = []

    if not disable_cache:
        try:
            data = get_cached_data(directory)
            if required_files:
                files_count = len(data.groupby(level=0))
                if files_count == required_files:
                    #print("Using cached data for {}".format(directory))
                    return data
                elif files_count > required_files:
                    #print("Using sliced cached data for {}".format(directory))
                    columns_count = len(data.groupby(level=1))
                    return data.ix[0:required_files * columns_count]
                else:
                    print(
                        "Warning, regenerationg cache for {} as it contain files only {} ({} requested)"
                        .format(directory, files_count, required_files))
            else:
                return data
        except IOError:
            pass

    for _, _, result_files in os.walk(directory):
        for filename in result_files:
            if extension(filename) != '.csv':
                continue
            result_file = os.path.join(directory, filename)
            try:
                data = get_data(result_file)
            except Exception as ex:
                print("Failed to read {}".format(result_file))
                raise ex
            all_data.append(data)
            filenames.append(filename)
            if required_files and len(all_data) >= required_files:
                break

    if required_files and len(all_data) != required_files:
        raise RuntimeError("Couldn't load required amount ({}) of datafiles from {}".format(
            required_files, directory))

    if not all_data:
        raise RuntimeError("Failed to read any data for {}".format(directory))

    columns = list(all_data[0].columns)
    index = [[file for file in filenames for _ in range(len(columns))], len(filenames) * columns]
    data = pandas.DataFrame(pandas.concat(all_data, axis=1).values.T, index=index)

    if not disable_cache:
        generate_cache(directory, data)

    return data


def get_max_prevalence(data):
    return data.max(axis=0)[0]


def get_sum_incidences(data):
    return [
        data[data.shape[1] - 1].ix[filename, 'cumulative incidence']
        for filename, _ in data.groupby(level=0)
    ]


def get_params(directory):
    s1, s2 = os.path.basename(directory).split("_")
    alpha = float(s1)
    try:
        tau = int(s2)
        return alpha, tau
    except ValueError:
        try:
            beta = float(s2)
            return alpha, beta
        except:
            raise


def find_zeroed_tail_start(values):
    for k, val in enumerate(reversed(values == 0)):
        if val == False:
            break

    return len(values) - k


if __name__ == "__main__":
    for dirpath, _, result_files in os.walk(args.results_dir):

        if not result_files:
            continue

        maxes = {}
        sums = {}
        for filename in result_files:
            result_file = os.path.join(dirpath, filename)
            data = get_data(result_file)
            maxes[filename] = get_max_prevalence(data)
            sums[filename] = get_sum_incidences(data)

        print('{}\t{}\t{}'.format(dirpath, max(maxes.values()), max(sums.values())))
