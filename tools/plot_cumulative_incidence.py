from analyze import get_data_from_directory, find_zeroed_tail_start
from matplotlib import pyplot
import pandas
import os
import types
from common import update_defaults_from_args, decimal_range
import numpy
import argparse
import sys
import json
from typing import List


def save_fig(fig, path):
    os.makedirs(os.path.split(path)[0], exist_ok=True)
    fig.savefig(path)
    pyplot.close(fig)


def load_param_space(space) -> List[float]:
    if space['type'] == "array":
        return space['value']
    raise RuntimeError("Unknown param space: {}".format(space))


def get_data_directory(data_request_files, dataset, alpha, recovery, recovery_type):
    for data_request_file in data_request_files:
        with open(data_request_file) as inp:
            data_request = json.load(inp)
        if not data_request['dataset'] == dataset:
            continue

        if recovery_type not in data_request['param_space']:
            continue

        recovery_values = load_param_space(data_request['param_space'][recovery_type])
        if recovery not in recovery_values:
            continue

        alphas = load_param_space(data_request['param_space']['alpha'])
        if alpha not in alphas:
            continue

        data_directorty_mask = os.path.join(
            os.path.dirname(data_request_file), data_request['output'])
        return data_directorty_mask.format(alpha=alpha, recovery=recovery)
    print("Failed to read data for {} {}".format(alpha, recovery))


def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--plot_request", type=str, required=True, help='Path to file describing plot request')
    parser.add_argument(
        "--data_request",
        type=str,
        dest='data_requests',
        required=True,
        action='append',
        help='Paths to files describing results directory')

    args = parser.parse_args(argv)
    with open(args.plot_request) as inp:
        plot_request = json.load(inp)

    output_directory = os.path.dirname(args.plot_request)

    plot(plot_request, args.data_requests, output_directory)


def plot(plot_request, data_requests, output_directory):
    assert plot_request['type'] == 'plot_cumulative_incidence'
    defaults = plot_request['defaults']

    fig = pyplot.figure(figsize=(20, 10))
    title = plot_request['title']
    xlabel = "Time [h]"
    ylabel = "Cumulative incidence"
    fontsize = 12

    pyplot.title(title, fontsize=fontsize)
    pyplot.xlabel(xlabel, fontsize=fontsize)
    pyplot.ylabel(ylabel, fontsize=fontsize)

    for plot in plot_request['plots']:
        recovery_type = 'tau' if 'tau' in plot else 'beta'
        simulation_recovery = plot[recovery_type]
        alpha = plot['alpha']
        xunits = 15 * 60  # 15 min
        xtick_density = 10 * 60 * 60  # 10h
        xtick_unit = 1 * 60 * 60  # 1h
        xtick_label = '{:.0f}h'  # 1h

        if recovery_type == 'tau':
            recovery = "{:.0f}h".format(simulation_recovery / xtick_unit * xunits)
        else:
            recovery = simulation_recovery

        label = plot.get('label', defaults['label']).format(alpha=alpha, recovery=recovery)

        directory = get_data_directory(data_requests, plot_request['dataset'], alpha,
                                       simulation_recovery, recovery_type)
        data = get_data_from_directory(directory)
        all_plots = [
            data.ix[filename, 'cumulative incidence'] for filename, _ in data.groupby(level=0)
        ]
        results = pandas.concat(all_plots, axis=1)
        average = results.mean(axis=1)
        xx = numpy.arange(len(average))
        pyplot.plot(xx, average, label=label)

        ticks = xx[::int(xtick_density / xunits)]
        pyplot.xticks(
            ticks=ticks, labels=[xtick_label.format(x * xunits / xtick_unit) for x in ticks])

        print("done: {}".format(label))

    pyplot.legend()
    pyplot.grid()

    output_filename = os.path.join(output_directory, plot_request['output'])
    print("{} saved".format(output_filename))
    save_fig(fig, output_filename)
    pyplot.close(fig)

    # pyplot.show()


if __name__ == "__main__":
    main(sys.argv[1:])
