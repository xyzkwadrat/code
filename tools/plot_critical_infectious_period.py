from analyze import get_data_from_directory, get_sum_incidences
import os
from matplotlib import pyplot
import pandas
import types
from common import update_defaults_from_args, decimal_range
from fstr import fstr as fstring
import numpy


def get_defaults():
    defaults = types.SimpleNamespace()
    defaults.infection_rate_range = ['0.90']
    defaults.recovery_rate_range = decimal_range("1.00", "0.00", "-0.01")
    defaults.label = '$\\alpha={infect}$, $\\tau={recover}$'
    defaults.xlabel = 'Time t [hour]'
    defaults.ylabel = 'Mean prevalence'
    defaults.title = 'Mean prevalence over {results.shape[1]} runs'
    defaults.fontsize = 20
    defaults.results_dir = None
    return defaults


def plot(args):
    ax = pyplot.gca()
    timescale = args.timescale if 'timescale' in dir(args) else 1
    for infect in args.infection_rate_range:
        incidences = {}
        for recover in args.recovery_rate_range:
            results_dir = os.path.join(args.results_dir, fstring("{infect}_{recover}"))

            results = get_data_from_directory(results_dir)
            sums = get_sum_incidences(results)
            incidences[float(recover) * timescale] = numpy.mean(sums)
        x = sorted(incidences.keys())[:]
        y = [incidences[xx] for xx in x]

        color = next(ax._get_lines.prop_cycler)['color']
        pyplot.semilogy(x, y, '-', color=color, label=fstring(args.label))
        pyplot.semilogy(x, y, '.', color=color)

    if 'xlim' in dir(args):
        pyplot.xlim(args.xlim)

    if 'ylim' in dir(args):
        pyplot.ylim(args.ylim)

    pyplot.title(fstring(args.title), fontsize=args.fontsize)
    pyplot.xlabel(fstring(args.xlabel), fontsize=args.fontsize)
    pyplot.ylabel(fstring(args.ylabel), fontsize=args.fontsize)

    if 'xticks' in dir(args):
        ticks, labels = pyplot.xticks()
        new_ticks, new_labels = args.xticks(ticks, labels)
        pyplot.xticks(new_ticks, new_labels)

    if 'yticks' in dir(args):
        ticks, labels = pyplot.yticks()
        new_ticks, new_labels = args.yticks(ticks, labels)
        pyplot.yticks(new_ticks, new_labels)

    pyplot.legend()
    pyplot.grid()
    #print(incidences)


if __name__ == "__main__":
    defaults = get_defaults()
    args = update_defaults_from_args(defaults)
    plot(args)
    pyplot.show()
