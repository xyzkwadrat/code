import argparse
import numpy
from decimal import Decimal
import os


def merge_namespaces(base, changes):
    fields = [field for field in dir(changes) if not field.startswith('__')]
    for field in fields:
        val = getattr(changes, field)
        setattr(base, field, val)


def add_defaults_as_options(options, defaults):
    fields = [field for field in dir(defaults) if not field.startswith('__')]
    for field in fields:
        val = getattr(defaults, field)
        typ = str if val is None else type(val)
        required = True if val is None else False
        options.add_argument(f'--{field}', default=val, type=typ, required=required)


def update_defaults_from_args(defaults):
    options = argparse.ArgumentParser()
    add_defaults_as_options(options, defaults)
    return options.parse_args()


def decimal_range(min_val: str, max_val: str, step: str):
    return numpy.arange(Decimal(min_val), Decimal(max_val), Decimal(step))


try:
    from matplotlib import pyplot

    def plot_using_module(module, customization, fig=None):
        args = module.get_defaults()
        merge_namespaces(args, customization)

        if fig == None:
            fig = pyplot.figure(figsize=(20, 10))
        module.plot(args)

        return fig

    def save_fig(fig, path):
        os.makedirs(os.path.split(path)[0], exist_ok=True)
        fig.savefig(path)
        pyplot.close(fig)

    def plot_and_save_using_module(module, customization, path):
        fig = plot_using_module(module, customization)
        save_fig(fig, path)
except:
    pass
