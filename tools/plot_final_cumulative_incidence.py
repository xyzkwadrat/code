import argparse
from decimal import Decimal
from typing import List
import numpy
from matplotlib import pyplot
import os
import json
import sys
import dacite
import typing

from tools.utils.data_request_resolver import ResultsReader
from tools.utils.requests import FigureRequest

import pathlib


def save_fig(fig, path):
    os.makedirs(os.path.split(path)[0], exist_ok=True)
    fig.savefig(path)
    pyplot.close(fig)


def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("--figure_request",
                        type=str,
                        required=True,
                        help='Path to file describing plot request')
    parser.add_argument("--results_database",
                        type=str,
                        dest='results_database',
                        required=True,
                        help='Paths to files describing results directory')
    parser.add_argument("--output_directory",
                        type=str,
                        default=".",
                        help='Path to root plots directory')

    args = parser.parse_args(argv)
    with open(args.figure_request) as inp:
        try:
            data = json.load(inp)
            figure_request = dacite.from_dict(data_class=FigureRequest,
                                              data=data,
                                              config=dacite.Config(strict=True))
        except Exception as e:
            raise RuntimeError(f"Could not load figure request from {args.figure_request}") from e
    reader = ResultsReader(rootDirectory=args.results_database, dataset=figure_request.dataset)

    output_filename = os.path.join(args.output_directory, os.path.dirname(args.figure_request),
                                   figure_request.output)

    plotRequest(figure_request, reader, output_filename)


def plotRequest(figure_request: FigureRequest, resultsReader: ResultsReader, output_filename: str):
    if figure_request.type != pathlib.Path(__file__).with_suffix('').name:
        raise RuntimeError(f"{__file__} is not designed to plot {figure_request.type}")

    alphas = figure_request.alpha.asArray()
    defaults = figure_request.defaults

    fig = pyplot.figure(figsize=(20, 10))
    title = figure_request.title
    xlabel = "$\\alpha$"
    ylabel = "Final cumulative incidence (total range)"
    fontsize = 12

    pyplot.title(title, fontsize=fontsize)
    pyplot.xlabel(xlabel, fontsize=fontsize)
    pyplot.ylabel(ylabel, fontsize=fontsize)

    for plot in figure_request.plots:
        recoveryString = Decimal(plot.format.format(plot.recovery))

        if plot.simulator == 'tau':
            recovery = "{:.1f}h".format(recoveryString / 4)
        else:
            recovery = str(recoveryString)

        final_vals = []
        for alpha in alphas:
            final_val = resultsReader.getAveragedFinalValue(recovery=Decimal(recoveryString),
                                                            alpha=alpha,
                                                            simulator=plot.simulator,
                                                            dataColumn=2)
            final_vals.append(final_val)

        pyplot.plot(alphas, final_vals, label=plot.label)

        print("done: {}".format(recovery))

    pyplot.legend()
    pyplot.grid()

    print("{} saved".format(output_filename))
    save_fig(fig, output_filename)
    pyplot.close(fig)


if __name__ == "__main__":
    main(sys.argv[1:])
