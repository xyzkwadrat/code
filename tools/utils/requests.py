from dataclasses import dataclass, fields
from decimal import Decimal
from typing import List, Iterable, Dict, Any, Dict, Union, Optional, TypeVar, Generic, Type, NewType
import numpy

T = TypeVar('T', covariant=True)

Dataset = NewType('Dataset', str)
Simulator = NewType('Simulator', str)


@dataclass(repr=True)
class ParamRange(Generic[T]):
    type: str
    start: T
    stop: T
    step: T
    format: str

    def asArray(self) -> numpy.array:
        start = Decimal(self.format.format(self.start))
        stop = Decimal(self.format.format(self.stop))
        step = Decimal(self.format.format(self.step))
        return numpy.arange(start, stop, step, dtype=Decimal)


@dataclass(repr=True)
class ParamArray(Generic[T]):
    type: str
    value: List[T]

    def asArray(self) -> numpy.array:
        return numpy.array(self.value)


@dataclass(repr=True)
class PlotDescription():
    label: Optional[str]
    format: Optional[str]
    simulator: Optional[Simulator]


@dataclass(repr=True)
class PlotRequest(PlotDescription):
    recovery: Union[int, float]

    def consumeDefaults(self, defaults: PlotDescription) -> None:
        self.label = self.label or defaults.label
        self.format = self.format or defaults.format
        self.simulator = self.simulator or defaults.simulator

    def sanitize(self):
        for field in fields(PlotDescription):
            if getattr(self, field.name) is None:
                raise RuntimeError(f"Missing field: {field.name}")


ParamSpace = Union[Union[ParamArray[int], ParamRange[int]],
                   Union[ParamArray[float], ParamRange[float]]]


@dataclass(repr=True)
class FigureRequest():
    version: str
    type: str
    dataset: Dataset
    title: str
    defaults: Optional[PlotDescription]
    alpha: ParamSpace
    plots: List[PlotRequest]
    output: str

    def __post_init__(self):
        for plot in self.plots:
            if self.defaults:
                plot.consumeDefaults(self.defaults)
            plot.sanitize()


@dataclass(repr=True)
class DataRequest():
    version: str
    name: str
    code_name: str
    dataset: Dataset
    simulator: Simulator
    start_time: int
    end_time: int
    repeats: int
    alpha: ParamSpace
    recovery: ParamSpace
    output: str
    timestep: str
