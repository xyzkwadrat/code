import sys
import json
import sys
import os
import argparse
from decimal import Decimal
from typing import List, Iterable, Dict, Any, Dict, Union, Optional, TypeVar, Generic, Type, Tuple, NewType
import numpy
import h5py
import dacite
import pathlib
from .requests import DataRequest, Dataset, Simulator


def walkFilesWithExtension(directory: pathlib.Path, extension: str) -> Iterable[str]:
    for root, _, result_files in os.walk(directory):
        relative = root[len(str(directory)) + 1:]
        for filename in result_files:
            if filename.endswith(extension):
                yield os.path.join(relative, filename)


def loadRequestFromDirectory(directory: pathlib.Path) -> Dict[str, DataRequest]:
    requests: Dict[str, DataRequest] = {}
    for request_file in walkFilesWithExtension(directory, '.json'):
        try:
            full_path = os.path.join(directory, request_file)
            with open(full_path) as buffer:
                data = json.load(buffer)
                requests[request_file] = dacite.from_dict(data_class=DataRequest,
                                                          data=data,
                                                          config=dacite.Config(strict=True))
        except Exception as ex:
            raise RuntimeError(f'Failed to parse {full_path}') from ex
    return requests


def getDataDirectory(request: DataRequest, recovery: Decimal, alpha: Decimal) -> pathlib.Path:
    return pathlib.Path(request.output.format(recovery=recovery, alpha=alpha))


def H5PyLoader(file):
    return h5py.File(file, 'r')


class ResultsReader(object):
    def __init__(self, rootDirectory: pathlib.Path, dataset: Dataset, fileLoader=H5PyLoader):
        self._rootDirectory = pathlib.Path(rootDirectory)
        self._dataset = dataset
        self._relatedRequests: Dict[Simulator, List[Tuple[pathlib.Path, DataRequest]]] = {}
        self._loader = fileLoader

        self._loadRelatedRequests()

    def _loadRelatedRequests(self):
        for file, request in loadRequestFromDirectory(self._rootDirectory).items():

            if request.dataset == self._dataset:
                if request.simulator not in self._relatedRequests:
                    self._relatedRequests[request.simulator] = []
                self._relatedRequests[request.simulator].append((file, request))

    def getAveragedFinalValue(self,
                              recovery: Decimal,
                              alpha: Decimal,
                              simulator: Simulator,
                              dataColumn: Union[str, int],
                              maxSamples: Optional[int] = None) -> float:
        loadedSamples: int = 0
        averages: List[float] = []

        skippedRequests: List[Tuple[str, str]] = []

        for file, request in self._relatedRequests[simulator]:
            if alpha not in request.alpha.asArray():
                skippedRequests.append(
                    (str(self._rootDirectory / file), f"alpha {alpha} not in range"))
                continue

            if recovery not in request.recovery.asArray():
                skippedRequests.append(
                    (str(self._rootDirectory / file), f"recovery {recovery} not in range"))
                continue

            if maxSamples and request.repeats + loadedSamples > maxSamples:
                samplesToLoad = maxSamples - request.repeats - loadedSamples
            else:
                samplesToLoad = request.repeats

            dataDir: pathlib.Path = self._rootDirectory / pathlib.Path(
                file).parent / getDataDirectory(request, recovery, alpha)

            globbedFiles: List[pathlib.Path] = []
            for h5file in dataDir.glob('*.h5'):
                data = self._loader(h5file)
                final_vals = data['data'][dataColumn, -1, :samplesToLoad]

                samplesCount = len(final_vals)
                averages.append(sum(final_vals) / samplesCount)
                samplesToLoad -= samplesCount
                loadedSamples += samplesCount

                globbedFiles.append(h5file)

                if not samplesToLoad:
                    break

            if not globbedFiles:
                raise RuntimeError(f"Could not find any .h5 file in {str(dataDir)}")

        if not averages:
            raise RuntimeError(
                f"No data files for simulator: {simulator}, alpha: {alpha}, recovery: {recovery}. Skipped: {skippedRequests}"
            )
        return sum(averages) / len(averages)


def parse_args(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument("--request_file", type=str, required=True, help="Path to data request file")
    return parser.parse_args(argv)


def main(argv):
    args = parse_args(argv)


if __name__ == "__main__":
    main(sys.argv[1:])
