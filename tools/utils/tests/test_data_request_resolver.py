import os
from ..data_request_resolver import walkFilesWithExtension, loadRequestFromDirectory, DataRequest, ResultsReader
import logging
from decimal import Decimal

TEST_DIR = os.path.join(os.path.dirname(__file__), "results")


class AnyMatcher(object):
    def __eq__(self, other):
        return True


def test_loadRequestFromDirectory():
    files = loadRequestFromDirectory(TEST_DIR)
    assert len(files) == 2
    assert {'A/tau.json': AnyMatcher(), 'B/tau.json': AnyMatcher()} == files
    atau: DataRequest = files['A/tau.json']
    assert atau.alpha.start == 1.00
    assert atau.alpha.step == -0.01
    assert type(atau.repeats) is int


def test_walkFilesWithExtension():
    files = list(walkFilesWithExtension(TEST_DIR, ".json"))
    assert sorted(files) == ['A/tau.json', 'B/tau.json']


def test_realData():
    reader = ResultsReader(rootDirectory='/data/work/mgr/resultsh5', dataset='conference')
    reader.getAveragedFinalValue(recovery=25, alpha=Decimal('0.997'), simulator='tau', dataColumn=2)