import os
from analyze import extension, read_header
from decimal import Decimal
import numpy
import io
from collections import defaultdict


def get_seeds(directory):
    seeds = defaultdict(list)
    for root, _, result_files in os.walk(directory):
        print('Lightening ' + root + '...', end='')
        for filename in result_files:
            if extension(filename) != '.csv':
                continue
            result_file = os.path.join(root, filename)
            seed = int(read_header(result_file)['seed'])
            seeds[seed].append(result_file)
        print(' done!')
    return seeds


def get_seeds_map(files):
    seeds = defaultdict(list)
    for filename in files:
        if extension(filename) != '.csv':
            continue
        seed = int(read_header(filename)['seed'])
        seeds[seed].append(filename)
    return dict(seeds)


def get_duplicated_seeds(seeds_map):
    return [seeds for seeds, files in seeds_map.items() if len(files) > 1]


def print_directories_with_duplicates(root_dir):
    visited_parents = []
    for root, _, files in os.walk(root_dir):
        if any([parent in root for parent in visited_parents]):
            continue
        csv_files = [filename for filename in files if filename.endswith('.csv')]
        if root is None or root == root_dir or len(csv_files) == 0:
            continue

        print(f"Checking {root}")
        try:
            file_paths = [os.path.join(root, filename) for filename in csv_files]
            seeds_map = get_seeds_map(file_paths)
            visited_parents.append(os.path.split(root)[0] + os.path.sep)
            seeds_count = len(seeds_map.keys())
            files_count = len(csv_files)
            if seeds_count != files_count:
                print(f'WARNING! {root} contains {seeds_count} seeds over {files_count} files')

        except KeyError:
            print(root, ' has no seed info, skipping')
            visited_parents.append(os.path.split(root)[0])
    return visited_parents


if __name__ == '__main__':
    import sys
    print_directories_with_duplicates(sys.argv[1])
