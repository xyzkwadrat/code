import numpy
from matplotlib import pyplot
# matlab formula
# eps = 0.05; tau = 0:100; beta = 1 - power(eps, 1./tau); [tau', beta']
from beta_in_tau import calculate_beta


def plot(args):
    xx = range(1, 120)
    eps = 0.05
    tau = 100
    beta = calculate_beta(eps, tau)
    beta_values = [(1 - beta)**t for t in xx]
    tau_values = [1 if t < tau else 0 for t in xx]
    pyplot.plot(xx, beta_values, label="Probability for $\\beta$ model")
    pyplot.plot(xx, tau_values, label="Probability for $\\tau$ model")

    pyplot.axhline(y=eps, ls='--', color='k')

    locs, labels = pyplot.yticks()
    labels = ['{0:.2f}'.format(loc) for loc in locs] + ['$\\epsilon$']
    locs = list(locs) + [eps]
    pyplot.yticks(locs, labels)

    pyplot.ylabel('Probability for being infected')
    pyplot.xlabel('Time')
    pyplot.title(
        f'Comparision of infection probability after time t in $\\beta$ and $\\tau$ based models with tolerance $\\epsilon={eps}$'
    )
    pyplot.xlim(xx[0], xx[-1])
    pyplot.legend()


if __name__ == "__main__":

    for tau in range(1, 100):
        print(tau, calculate_beta(0.05, tau), calculate_beta(0.01, tau))
    print(calculate_beta(0.01, 91))
    print(calculate_beta(0.01, 80))
    print(calculate_beta(0.05, 80))

    K = 90
    eps = numpy.float(0.05)
    tau = numpy.arange(1, K + 1, 1, dtype=numpy.int32)
    beta = calculate_beta(eps, tau)
    pyplot.plot(tau, beta, '.')
    pyplot.grid()
    #errbars = pyplot.errorbar(tau, beta, yerr=([0]*K, 1-beta), linestyle="None")
    pyplot.xlabel("$\\tau$")
    pyplot.ylabel("$\\beta$")
    pyplot.title(
        "Function of lower bound of $\\beta$ in $\\tau$ for error $\\epsilon={}$".format(eps))

    pyplot.close()
    pyplot.figure()
    plot(None)
    pyplot.show()
