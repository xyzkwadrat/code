def calculate_beta(eps, tau):
    return 1.0 - eps**(1 / tau)
