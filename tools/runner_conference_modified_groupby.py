from runner import *

# python cpp/tools/runner_conference_modified_groupby.py --result_dir=tmp2 --input_file=data/ht09_contact_list.dat --conference_group_by_seconds=900


def get_modified_tau_span(base_tau_span, multiplier):
    if base_tau_span[0] is None:
        return [None]
    modified_taus = numpy.unique(numpy.array(base_tau_span / multiplier, dtype=int))
    tau_span = [tau for tau in modified_taus if tau > 0]
    return tau_span


def get_modified_beta(beta, multiplier):
    return 1 - pow(1 - beta, 1 / multiplier)


def get_modified_beta_span(base_beta_span, multiplier):
    if base_beta_span[0] is None:
        return [None]

    return [get_modified_beta(float(beta), multiplier) for beta in base_beta_span]


def main():
    args = options()
    if not args.dry_run:
        assert is_executable(args.app_path), f"{args.app_path} is not executable"
        assert guessDataFormat(
            args.input_file
        ) == "ConferenceContacts", f"{args.input_file} is not conference contacts file"

    alpha_span = numpy.arange(Decimal('1.00'), Decimal('0'), Decimal('-0.10'), dtype=Decimal)
    base_tau_span = [None]  # numpy.arange(100, 0, -1)
    base_beta_span = numpy.arange(Decimal('0.020'), Decimal('0'), Decimal('-0.001'), dtype=Decimal)

    base_beta_span = numpy.arange(Decimal('0.10'), Decimal('0'), Decimal('-0.01'), dtype=Decimal)

    results_dir = args.result_dir
    if args.conference_group_by_seconds:
        base_parameter = args.conference_group_by_seconds
    else:
        base_parameter = 15 * 60

    for multiplier in [0.25, 0.5, 1, 2, 3, 4, 5]:
        tau_span = get_modified_tau_span(base_tau_span, multiplier)
        beta_span = get_modified_beta_span(base_beta_span, multiplier)
        groupby_parameter = int(base_parameter * multiplier)

        #print(multiplier, groupby_parameter, tau_span, base_beta_span, beta_span)
        args.result_dir = os.path.join(results_dir, str(groupby_parameter))
        args.conference_group_by_seconds = groupby_parameter

        run_plain_for_alpha_beta_tau(alpha_span, tau_span, beta_span, 50, args)


if __name__ == "__main__":
    main()
