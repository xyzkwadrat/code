#! /usr/bin/env python
# -*- coding: utf-8 -*-

# This simple example on how to do animations using graph-tool. Here we do a
# simple simulation of an S->I->R->S epidemic model, where each vertex can be in
# one of the following states: Susceptible (S), infected (I), recovered (R). A
# vertex in the S state becomes infected either spontaneously with a probability
# 'x' or because a neighbor is infected. An infected node becomes recovered
# with probability 'r', and a recovered vertex becomes again susceptible with
# probability 's'.

# DISCLAIMER: The following code is definitely not the most efficient approach
# if you want to simulate this dynamics for very large networks, and/or for very
# long times. The main purpose is simply to highlight the animation capabilities
# of graph-tool.

from graph_tool.all import *
from numpy.random import *
import sys, os, os.path
import itertools
import collections
# We need some Gtk and gobject functions
from gi.repository import Gtk, Gdk, GdkPixbuf, GObject

# We will use the network of network scientists, and filter out the largest
# component

g = Graph(directed=False)
data = []
all_edges = set()


def read_pairs(path):
    with open(path) as f:
        lines = f.readlines()
        return [tuple(sorted(map(int, line.split(',')))) for line in lines if line]


sources = '../../results_conference_full/1_110/16042018-001335/{}.triplets'
for k in itertools.count():
    try:
        pairs = read_pairs(sources.format(k))
        data.append(pairs)
        all_edges.update(set(pairs))
    except FileNotFoundError:
        break

print(all_edges)
for left, right in all_edges:
    g.add_edge(left, right)

pos = fruchterman_reingold_layout(g, n_iter=1000)
pos = arf_layout(g)
pos = sfdp_layout(g)
#gt.graph_draw(g, pos=pos, output="graph-draw-fr.pdf")

# The states would usually be represented with simple integers, but here we will
# use directly the color of the vertices in (R,G,B,A) format.

S = [1, 1, 1, 1]  # White color
I = [0, 0, 0, 1]  # Black color
R = [0.5, 0.5, 0.5, 1.]  # Grey color (will not actually be drawn)

# Initialize all vertices to the S state
state = g.new_vertex_property("vector<double>")
for v in g.vertices():
    state[v] = S

# Newly infected nodes will be highlighted in red
newly_infected = g.new_vertex_property("bool")
visible = g.new_edge_property("bool")
visible[g.edges().next()] = False

indexes_map = collections.defaultdict(dict)
for edge in g.edges():
    indexes_map[int(edge.source())][int(edge.target())] = edge

for left, right in data[0]:
    visible[indexes_map[left][right]] = True

win = GraphWindow(
    g,
    pos,
    geometry=(1600, 900),
    edge_color=[0.6, 0.6, 0.6, 1],
    vertex_fill_color=state,
    vertex_halo=newly_infected,
    vertex_halo_color=[0.8, 0, 0, 0.6])

step = 0


def update_state():
    global step
    step += 1
    print(step)
    #newly_infected.a = False
    visible.a = False
    for left, right in data[step // 10]:
        visible[indexes_map[left][right]] = True

    state[v] = S
    #if state[v] == R:
    #    removed[v] = True

    # Filter out the recovered vertices
    # g.set_vertex_filter(removed, inverted=True)
    g.set_edge_filter(visible)

    # The following will force the re-drawing of the graph, and issue a
    # re-drawing of the GTK window.
    win.graph.regenerate_surface()
    win.graph.queue_draw()

    # We need to return True so that the main loop will call this function more
    # than once.
    return True


# Bind the function above as an 'idle' callback.
cid = GObject.idle_add(update_state)

# We will give the user the ability to stop the program by closing the window.
win.connect("delete_event", Gtk.main_quit)

# Actually show the window, and start the main loop.
win.show_all()
Gtk.main()
