import os
from analyze import extension, read_header
from decimal import Decimal
import numpy
import io


def get_line_data(line, delim=';'):
    return numpy.array([Decimal(entry) for entry in line.split(delim)])


def all_zeros(array):
    return not array.any()


def lighten_data(stream_in, stream_out):
    for line in stream_in:
        if line.startswith('#') or not all_zeros(get_line_data(line)):
            stream_out.write(line)
        else:
            break


def lighten_directory(directory):
    for root, _, result_files in os.walk(directory):
        print('Lightening ' + root + '...', end='')
        for filename in result_files:
            if extension(filename) != '.csv':
                continue
            result_file = os.path.join(root, filename)
            if not os.path.isfile(result_file + '.bak'):
                try:
                    stream_out = io.StringIO()
                    with open(result_file) as stream_in:
                        lighten_data(stream_in, stream_out)
                    os.rename(result_file, result_file + '.bak')
                    with open(result_file, 'w') as output:
                        output.write(stream_out.getvalue())
                except Exception as ex:
                    print("Failed to lighten {}".format(result_file))
                    raise ex
        print(' done!')


if __name__ == '__main__':
    import sys
    lighten_directory(sys.argv[1])
