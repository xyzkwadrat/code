from analyze import get_data_from_directory, get_params
import os
from matplotlib import pyplot
import pandas
from decimal import Decimal
import numpy
import types
from common import update_defaults_from_args, decimal_range
from fstr import fstr as fstring


def get_defaults():
    defaults = types.SimpleNamespace()
    defaults.infection_rate_range = decimal_range('1.00', '0.0', '-0.01')
    defaults.recovery_rate = '80'
    defaults.label = '$\\alpha={infect}$, $\\tau={recover}$'
    defaults.xlabel = 'Infection rate $\\alpha$'
    defaults.ylabel = 'Mean prevalence'
    defaults.title = 'Mean prevalence for {results.shape[1]} permutations with $\\tau={recover}$'
    defaults.fontsize = 20
    defaults.results_dir = None
    return defaults


def plot(args):
    recover = args.recovery_rate
    alpha_span = args.infection_rate_range
    values = []

    for infect in alpha_span:
        result_dir = os.path.join(args.results_dir, fstring("{infect}_{recover}"))
        data = get_data_from_directory(result_dir)
        all_plots = [data.ix[filename, 'meanPrevalence'] for filename, _ in data.groupby(level=0)]
        results = pandas.concat(all_plots, axis=1)
        average = results.mean(axis=1)
        values.append(average.max())

    pyplot.plot(alpha_span, values)
    # pyplot.scatter(alpha_span, values, 'r')

    pyplot.title(fstring(args.title), fontsize=args.fontsize)
    pyplot.xlabel(fstring(args.xlabel), fontsize=args.fontsize)
    pyplot.ylabel(fstring(args.ylabel), fontsize=args.fontsize)


#    pyplot.title.format(len(all_plots), alpha, tau))

if __name__ == "__main__":
    defaults = get_defaults()
    args = update_defaults_from_args(defaults)
    plot(args)
    pyplot.show()
