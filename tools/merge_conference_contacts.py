import argparse
import sys
from typing import Iterable, List, NamedTuple, Dict, Tuple

Timestamp = int
PersonId = int


class ContactEvent(NamedTuple):
    timestamp: Timestamp
    firstId: PersonId
    secondId: PersonId


class Contact(NamedTuple):
    startTime: Timestamp
    endTime: Timestamp
    firstId: PersonId
    secondId: PersonId

    @property
    def duration(self):
        return self.endTime - self.startTime


# pylint seems to fail to accept that List is list
class ContactsList(List[Contact]):
    def __init__(self):
        super().__init__(self)

    @property
    def allIds(self):
        ids = set()
        for contact in self:  # pylint: disable=not-an-iterable
            ids.add(contact.firstId)
            ids.add(contact.secondId)
        sortedIds = [i for i in ids]
        return sorted(sortedIds)


def loadContactEvents(lines: Iterable[str]) -> List[ContactEvent]:
    values = []
    for line in lines:
        timestamp, idLeft, idRight = line.split()
        idMin, idMax = sorted((idLeft, idRight))

        values.append(ContactEvent(Timestamp(timestamp), PersonId(idMin), PersonId(idMax)))
    return values


def convertTimestampUnits(events: List[ContactEvent], multiplier: float) -> List[ContactEvent]:
    return [
        ContactEvent(Timestamp(event.timestamp * multiplier), event.firstId, event.secondId)
        for event in events
    ]


def groupByTimestamps(events: List[ContactEvent]) -> List[List[ContactEvent]]:
    groups = []
    group = []  # type: List[ContactEvent]
    for event in events:
        if not group or group[0].timestamp == event.timestamp:
            group.append(event)
        else:
            groups.append(group)
            group = [event]

    if group:
        groups.append(group)

    return groups


def groupByDuration(container: List[Contact]) -> List[List[Contact]]:
    groups = []
    group = []  # type: List[Contact]
    for elem in container:
        if not group or group[0].duration == elem.duration:
            group.append(elem)
        else:
            groups.append(group)
            group = [elem]

    if group:
        groups.append(group)

    return groups


def mergeContactGroups(groups: List[List[ContactEvent]]) -> ContactsList:
    contacts = ContactsList()
    recentContacts = {}  # type: Dict[Tuple[PersonId, PersonId], ContactEvent]
    recentTimestamp = 0
    timeDelta = 20
    for group in groups:
        currentTimestamp = group[0].timestamp

        activeContacts = {}  # type: Dict[Tuple[PersonId, PersonId], ContactEvent]

        if currentTimestamp - recentTimestamp <= timeDelta:
            for event in group:
                ids = (event.firstId, event.secondId)
                if ids in recentContacts:
                    activeContacts[ids] = recentContacts.pop(ids)
                elif ids not in activeContacts:
                    activeContacts[ids] = event

        for event in recentContacts.values():
            contacts.append(  # pylint: disable=no-member
                Contact(event.timestamp, currentTimestamp, event.firstId, event.secondId))

        recentTimestamp = currentTimestamp
        recentContacts = activeContacts

    return contacts


def parseArgs(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('--input_file', type=str, help='Path to ht09 dat file')
    parser.add_argument(
        '--timestamp_divisor',
        type=int,
        help='Divisor of timestamp, can be used to merge multiple events',
        default=900)
    parser.add_argument('--tau', type=int, help='tau in number of iterations', default=80)
    return parser.parse_args(argv)


def printContactsByDuration(contacts: List[Contact]):

    contacts.sort(key=lambda c: c.duration)

    contactsByDuration = groupByDuration(contacts)

    print("Contacts by duration:\n[Duration] [Amount]")
    for group in contactsByDuration:
        print(group[0].duration, len(group))


def getContactsUntilTau(personId: PersonId, tau: int, contacts: List[Contact]) -> List[Contact]:
    contacts.sort(key=lambda c: c.startTime)
    filteredContacts = []
    for contact in contacts:
        if contact.startTime > tau:
            break
        if personId in (contact.firstId, contact.secondId):
            filteredContacts.append(contact)
    return filteredContacts


def getFirstContact(personId: PersonId, contacts: List[Contact]) -> Contact:
    contacts.sort(key=lambda c: c.startTime)
    for contact in contacts:
        if personId in (contact.firstId, contact.secondId):
            return contact
    raise LookupError(f"Missing id {personId}")


def fractionOfContactsBeforeTau(contacts: ContactsList, tau: int) -> float:
    allIds = contacts.allIds
    count = 0
    for i in allIds:
        start = getFirstContact(i, contacts).startTime
        if start <= tau:
            count += 1
    return count / len(allIds)


def main(argv):
    args = parseArgs(argv)
    with open(args.input_file) as inp:
        events = loadContactEvents(inp)

    events = convertTimestampUnits(events, 1 / args.timestamp_divisor)
    groups = groupByTimestamps(events)
    contacts = mergeContactGroups(groups)

    allIds = contacts.allIds
    idsCount = len(allIds)

    smallerThanTauAmount = 0
    for i in allIds:
        start = getFirstContact(i, contacts).startTime
        if start <= args.tau:
            smallerThanTauAmount += 1

    print(f"Number of ids with first contact before tau={args.tau}: {smallerThanTauAmount}, "
          f"{smallerThanTauAmount/idsCount * 100}% of {idsCount}")

    for i in allIds:
        contactsBeforeTau = getContactsUntilTau(i, args.tau, contacts)
        durations = ', '.join((str(c.duration) for c in contactsBeforeTau))
        print(f"Id {i} has {len(contactsBeforeTau)} contacts before {args.tau} "
              f"with durations: {durations}")

    print('')
    printContactsByDuration(contacts)

    contacts.sort(key=lambda c: c.startTime)  # pylint: disable=no-member
    for tau in range(1, 101):
        print(f"{tau}: {fractionOfContactsBeforeTau(contacts, tau)}, ")


if __name__ == "__main__":
    main(sys.argv[1:])
