from analyze import get_data_from_directory, find_zeroed_tail_start
from matplotlib import pyplot
import pandas
import os
from fstr import fstr as fstring
import types
from common import update_defaults_from_args, decimal_range


def get_defaults():
    defaults = types.SimpleNamespace()
    defaults.infection_rate_range = decimal_range('1.00', '0.60', '-0.10')
    defaults.recovery_rate_range = decimal_range('1', '100', '1')
    defaults.label = '$\\alpha={infect}$'
    defaults.xlabel = 'Tau [day]'
    defaults.ylabel = 'Max of mean prevalence'
    defaults.title = 'Peaks of mean prevalence over {results.shape[1]} runs'
    defaults.fontsize = 20
    defaults.results_dir = None
    return defaults


def plot(args):
    for infect in args.infection_rate_range:
        maximas = []
        for recover in args.recovery_rate_range:
            results_dir = os.path.join(args.results_dir, fstring("{infect}_{recover}"))
            data = get_data_from_directory(results_dir)
            all_plots = [
                data.ix[filename, 'meanPrevalence'] for filename, _ in data.groupby(level=0)
            ]
            results = pandas.concat(all_plots, axis=1)
            average = results.mean(axis=1)
            maximum = average.max()
            maximas.append(maximum)
            #print(fstring("{infect}_{recover}: {maximum}"))

        pyplot.plot(args.recovery_rate_range, maximas, label=fstring(args.label))

    if 'xlim' in dir(args):
        pyplot.xlim(args.xlim)

    if 'ylim' in dir(args):
        pyplot.ylim(args.ylim)

    pyplot.title(fstring(args.title), fontsize=args.fontsize)
    pyplot.xlabel(fstring(args.xlabel), fontsize=args.fontsize)
    pyplot.ylabel(fstring(args.ylabel), fontsize=args.fontsize)
    pyplot.legend()
    pyplot.grid()


if __name__ == "__main__":
    defaults = get_defaults()
    args = update_defaults_from_args(defaults)
    plot(args)
    pyplot.show()
