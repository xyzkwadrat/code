import os

from bootstrap import bootstrap
results_dir, parent_dir, script_name = bootstrap(__file__)

from results_sex import *


def main(results_dir, parent_dir, script_name):
    custom = get_common_config()
    custom.infection_rate_range = ['1.00']
    taus = list(range(1, 100)) + list(range(100, 151, 10))
    for tau in taus:
        break
        custom.title = f"Mean prevalence in function of $\\alpha$ for $\\tau={tau}$ " + " over {results.shape[1]} permuted runs"
        custom.recovery_rate_range = [tau]
        custom.label = '$\\alpha={infect}$, $\\tau={recover}$'

        mean_prevalence(custom, results_dir, parent_dir, script_name + f'\\in_alpha_for_tau_{tau}')

    custom.recovery_rate_range = range(1, 100)
    custom.infection_rate_range = numpy.arange(Decimal('1.00'), Decimal('0.0'), Decimal('-0.10'))
    custom.xlabel = 'tau [day]'
    custom.ylabel = 'Final cumulative incidence'
    custom.title = f"Cumulative incidence for different $\\alpha$ in semi-log scale"
    custom.label = '$\\alpha={infect}$'
    critical_infection(custom, results_dir, parent_dir, script_name)

    custom.recovery_rate_range = range(1, 100)
    custom.xlabel = 'tau [day]'
    peak_mean_prevalence(custom, results_dir, parent_dir, script_name)


if __name__ == '__main__':
    main(results_dir, parent_dir, script_name)
