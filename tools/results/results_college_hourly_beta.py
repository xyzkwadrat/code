import os

from bootstrap import bootstrap
results_dir, parent_dir, script_name = bootstrap(__file__)

from results_sex import *
from common import decimal_range

if __name__ == '__main__':

    betas = decimal_range('0.05', '0', '-0.01')
    alphas = decimal_range('1.00', '0.0', '-0.10')
    custom = get_common_config()
    for beta in betas:
        custom.title = f"Mean prevalence in function of $\\alpha$ for $\\beta={beta}$ " + " over {results.shape[1]} runs"
        custom.infection_rate_range = alphas
        custom.recovery_rate_range = [beta]
        custom.label = '$\\alpha={infect}$, $\\beta={recover}$'

        mean_prevalence(custom, results_dir, parent_dir,
                        script_name + f"\\in_alpha\\for_beta_{beta}")
        print(f"{beta} done")

    for alpha in alphas:
        custom.title = f"Mean prevalence in function of $\\beta$ for $\\alpha={alpha}$" + " over {results.shape[1]} runs"
        custom.infection_rate_range = [alpha]
        custom.recovery_rate_range = betas
        custom.label = '$\\alpha={infect}$, $\\beta={recover}$'

        mean_prevalence(custom, results_dir, parent_dir,
                        script_name + f"\\in_beta\\for_alpha_{alpha}")
        print(f"{alpha} done")
