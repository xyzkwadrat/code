import os

from bootstrap import bootstrap
results_dir, parent_dir, script_name = bootstrap(__file__)
from results_sex_tau import main

if __name__ == '__main__':
    main(results_dir, parent_dir, script_name)
