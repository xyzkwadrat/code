import os

from bootstrap import bootstrap
results_dir, parent_dir, script_name = bootstrap(__file__)

from results_sex import *
from common import decimal_range

if __name__ == '__main__':

    for betas in [decimal_range(1, 100, 1), decimal_range(1, 31, 1)]:
        custom = get_common_config()
        custom.timescale = 0.25
        custom.recovery_rate_range = betas
        custom.infection_rate_range = numpy.arange(
            Decimal('1.00'), Decimal('0.0'), Decimal('-0.10'))
        custom.xlabel = '$\\tau$ [hour]'
        custom.ylabel = 'Final cumulative incidence'
        custom.title = f"Cumulative incidence for different $\\alpha$ in semi-log scale"
        custom.label = '$\\alpha={infect}$'
        critical_infection(custom, results_dir, parent_dir,
                           script_name + f'-{betas[0]}-{betas[-1]}')

    exit(0)

    for tau in decimal_range(1, 100, 1):
        custom = get_common_config()
        custom.title = f"Mean prevalence in function of $\\alpha$ for $\\tau={tau}$ " + " over {results.shape[1]} runs"
        custom.infection_rate_range = decimal_range('1.00', '0.0', '-0.10')
        custom.recovery_rate_range = [tau]
        custom.label = '$\\alpha={infect}$, $\\beta={recover}$'

        mean_prevalence(custom, results_dir, parent_dir, script_name + f'\\in_alpha\\for_tau_{tau}')
