import os

from bootstrap import bootstrap
results_dir, parent_dir, script_name = bootstrap(__file__)

from results_sex import *
from common import decimal_range


def labels_divider(multiplier):
    def change_ticks(ticks, _):
        return ticks, [str(tick / multiplier) for tick in ticks]

    return change_ticks


if __name__ == '__main__':

    for betas in [decimal_range(1, 100, 1), decimal_range(1, 31, 1)]:
        custom = get_common_config()
        custom.timescale = 0.25
        custom.recovery_rate_range = betas
        custom.infection_rate_range = numpy.arange(
            Decimal('1.00'), Decimal('0.0'), Decimal('-0.10'))
        custom.xlabel = '$\\tau$ [hour]'
        custom.ylabel = 'Final cumulative incidence'
        custom.title = f"Cumulative incidence for different $\\alpha$ in semi-log scale"
        custom.label = '$\\alpha={infect}$'
        critical_infection(custom, results_dir, parent_dir,
                           script_name + f'-{betas[0]}-{betas[-1]}')

    exit(0)

    for tau in betas:
        custom = get_common_config()
        custom.timescale = 0.25
        custom.title = f"Mean prevalence in function of time for different $\\alpha$ with $\\tau={float(tau)*custom.timescale}h$ " + " over {results.shape[1]} runs"
        custom.infection_rate_range = decimal_range('1.00', '0.0', '-0.10')
        custom.recovery_rate_range = [tau]
        custom.label = '$\\alpha={infect}$'
        custom.xlabel = 'Time t [hour]'
        #        custom.xticks = labels_divider(4)

        mean_prevalence(custom, results_dir, parent_dir, script_name + f'\\in_alpha\\for_tau_{tau}')
