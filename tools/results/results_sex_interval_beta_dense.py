import os

from bootstrap import bootstrap
results_dir, parent_dir, script_name = bootstrap(__file__)

from results_sex import *
from common import decimal_range


def main(results_dir, parent_dir, script_name):
    custom = get_common_config()
    betas = decimal_range('0.020', '0.000', '-0.001')
    for beta in betas:
        break
        custom.title = f"Mean prevalence in function of $\\alpha$ for $\\beta={beta}$ " + " over {results.shape[1]} runs"
        custom.recovery_rate_range = [beta]
        #custom.recovery_rate_range = ['0.01', '0.02', '0.05', '0.10']
        custom.label = '$\\alpha={infect}$, $\\beta={recover}$'

        mean_prevalence(custom, results_dir, parent_dir,
                        script_name + f'\\in_alpha\\for_beta_{beta}')

    custom.recovery_rate_range = betas
    custom.xlabel = 'beta'
    custom.ylabel = 'Final cumulative incidence'
    custom.title = f"Cumulative incidence for different $\\alpha$ in semi-log scale"
    custom.label = '$\\alpha={infect}$'
    critical_infection(custom, results_dir, parent_dir, script_name)

    custom.recovery_rate_range = betas
    custom.xlabel = 'beta'
    peak_mean_prevalence(custom, results_dir, parent_dir, script_name)


if __name__ == '__main__':
    main(results_dir, parent_dir, script_name)
# model zerowy
