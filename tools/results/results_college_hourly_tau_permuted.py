import os

from bootstrap import bootstrap
results_dir, parent_dir, script_name = bootstrap(__file__)

from results_sex import *
from common import decimal_range

if __name__ == '__main__':

    taus = list(decimal_range('10', '200', '10')) + list(decimal_range('200', '500', '25'))
    alphas = decimal_range('1.00', '0.0', '-0.10')
    custom = get_common_config()
    for tau in taus:
        custom.title = f"Mean prevalence in function of $\\alpha$ for $\\tau={tau}$ " + " over {results.shape[1]} runs"
        custom.infection_rate_range = alphas
        custom.recovery_rate_range = [tau]
        custom.label = '$\\alpha={infect}$, $\\tau={recover}$'

        mean_prevalence(custom, results_dir, parent_dir, script_name + f"\\in_alpha\\for_tau_{tau}")
        print(f"{tau} done")

    zoomed_taus = decimal_range('1', '100', '1')
    for tau in zoomed_taus:
        custom.title = f"Mean prevalence in function of $\\alpha$ for $\\tau={tau}$ " + " over {results.shape[1]} runs"
        custom.infection_rate_range = alphas
        custom.recovery_rate_range = [tau]
        custom.label = '$\\alpha={infect}$, $\\tau={recover}$'
        custom.xlim = [0, 1000]
        custom.ylim = [0, 0.0175]

        mean_prevalence(custom, results_dir, parent_dir,
                        script_name + f"\\in_alpha\\1-99\\for_tau_{tau}")
        print(f"{tau} done")

    zoomed_taus = decimal_range('1', '51', '1')
    for tau in zoomed_taus:
        custom.title = f"Mean prevalence in function of $\\alpha$ for $\\tau={tau}$ " + " over {results.shape[1]} runs"
        custom.infection_rate_range = alphas
        custom.recovery_rate_range = [tau]
        custom.label = '$\\alpha={infect}$, $\\tau={recover}$'
        custom.xlim = [0, 1000]
        custom.ylim = [0, 0.005]

        mean_prevalence(custom, results_dir, parent_dir,
                        script_name + f"\\in_alpha\\1-50\\for_tau_{tau}")
        print(f"{tau} done")

    for alpha in alphas:
        custom.title = f"Mean prevalence in function of $\\tau$ for $\\alpha={alpha}$" + " over {results.shape[1]} runs"
        custom.infection_rate_range = [alpha]
        custom.recovery_rate_range = taus
        custom.label = '$\\alpha={infect}$, $\\tau={recover}$'

        mean_prevalence(custom, results_dir, parent_dir,
                        script_name + f"\\in_tau\\for_alpha_{alpha}")
        print(f"{alpha} done")
