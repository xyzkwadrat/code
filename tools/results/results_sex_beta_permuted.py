import os

from bootstrap import bootstrap
results_dir, parent_dir, script_name = bootstrap(__file__)

from results_sex import *
from common import decimal_range


def main(results_dir, parent_dir, script_name):
    custom = get_common_config()
    for beta in decimal_range('0.01', '0.05', '0.01'):
        break
        custom.title = f"Mean prevalence in function of $\\alpha$ for $\\beta={beta}$ " + " over {results.shape[1]} permuted runs"
        custom.recovery_rate_range = [beta]
        custom.label = '$\\alpha={infect}$, $\\beta={recover}$'

        mean_prevalence(custom, results_dir, parent_dir,
                        script_name + f'\\in_alpha_for_beta_{beta}')

    custom.recovery_rate_range = decimal_range('0.05', '0.00', '-0.01')
    custom.xlabel = 'beta'
    peak_mean_prevalence(custom, results_dir, parent_dir, script_name)


if __name__ == '__main__':
    main(results_dir, parent_dir, script_name)
