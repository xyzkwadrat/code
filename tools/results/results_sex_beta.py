import os

from bootstrap import bootstrap
results_dir, parent_dir, script_name = bootstrap(__file__)

from results_sex import *
from common import decimal_range


def main(results_dir, parent_dir, script_name):
    custom = get_common_config()
    betas = decimal_range('0.01', '0.05', '0.01')
    for beta in betas:
        break
        custom.title = f"Mean prevalence in function of $\\alpha$ for $\\beta={beta}$ " + " over {results.shape[1]} runs"
        custom.recovery_rate_range = [beta]
        #custom.recovery_rate_range = ['0.01', '0.02', '0.05', '0.10']
        custom.label = '$\\alpha={infect}$, $\\beta={recover}$'

        mean_prevalence(custom, results_dir, parent_dir,
                        script_name + f'\\in_alpha_for_beta_{beta}')

    custom.recovery_rate_range = list(decimal_range('0.001', '0.020', '0.001')) + list(
        decimal_range('0.02', '0.05', '0.01')) + list(decimal_range('0.05', '0.30', '0.05'))
    custom.infection_rate_range = numpy.arange(Decimal('1.00'), Decimal('0.0'), Decimal('-0.10'))
    custom.xlabel = '$\\beta$'
    custom.ylabel = 'Final cumulative incidence'
    custom.title = f"Cumulative incidence for different $\\alpha$ in semi-log scale"
    custom.label = '$\\alpha={infect}$'
    critical_infection(custom, results_dir, parent_dir, script_name)

    for alpha in decimal_range('1.00', '0.0', '-0.10'):
        custom.title = f"Mean prevalence in function of time for various $\\beta$ with $\\alpha={alpha}$ " + " over {results.shape[1]} runs"
        custom.infection_rate_range = [alpha]
        custom.recovery_rate_range = decimal_range('0.05', '0.00', '-0.01')
        custom.label = '$\\beta={recover}$'
        mean_prevalence(custom, results_dir, parent_dir,
                        script_name + f"\\in_beta\\for_alpha_{alpha}")


if __name__ == '__main__':
    main(results_dir, parent_dir, script_name)
# model zerowy
