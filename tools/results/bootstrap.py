import os
import sys

bootstrapped = False


def bootstrap(file):
    global bootstrapped
    results_dir = os.path.splitext(file)[0]
    parent_dir = os.path.split(os.path.splitext(file)[0])[0]
    script_name = os.path.split(os.path.splitext(file)[0])[1]
    tools_directory = os.path.join(parent_dir, '..', 'tools')
    if not bootstrapped:
        sys.path.append(tools_directory)
        bootstrapped = True
    return results_dir, parent_dir, script_name
