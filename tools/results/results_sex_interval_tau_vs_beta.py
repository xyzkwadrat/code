import os

from bootstrap import bootstrap
results_dir, parent_dir, script_name = bootstrap(__file__)
from common import plot_using_module, decimal_range, save_fig
import plot_mean_prevalence
import types
import numpy


def split_dirs_on_vs(results_dir):
    words = results_dir.split('_')
    tau_index = words.index('tau')
    base_words = words[0:tau_index]
    vs_index = words.index('vs')
    tau_words = words[tau_index:vs_index]
    beta_words = words[vs_index + 1:]
    tau = '_'.join(base_words + tau_words)
    beta = '_'.join(base_words + beta_words)
    return tau, beta


def plot_additively():
    plots_dir = os.path.join(parent_dir, module.__name__)
    path = os.path.join(plots_dir, script_name + ".png")
    customization.results_dir = results_dir


def get_common_config():
    custom = types.SimpleNamespace()
    custom.infection_rate_range = ['1.00', '0.70']  #decimal_range('1.00', '0.50', '-0.20')
    custom.xlabel = 'Time t [day]'
    custom.title = f"Mean prevalence in function of time for various $\\alpha$ with both $\\tau$ and $\\beta$ over " + "{results.shape[1]} runs"
    return custom


def main(results_dir, parent_dir, script_name):
    tau_dir, beta_dir = split_dirs_on_vs(results_dir)

    print(tau_dir, beta_dir)
    module = plot_mean_prevalence
    custom = get_common_config()

    tau = 90
    custom.recovery_rate_range = [tau]
    custom.label = '$\\alpha={infect}$, $\\tau={recover}$ days'
    custom.results_dir = tau_dir

    fig = plot_using_module(module, custom)

    beta = '0.01'
    custom.recovery_rate_range = [beta]
    custom.label = '$\\alpha={infect}$, $\\beta={recover}$'
    custom.results_dir = beta_dir

    fig = plot_using_module(module, custom, fig)

    plots_dir = os.path.join(parent_dir, module.__name__)
    path = os.path.join(plots_dir, script_name + ".png")

    save_fig(fig, path)


if __name__ == '__main__':
    main(results_dir, parent_dir, script_name)
