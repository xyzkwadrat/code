import types
import numpy
from decimal import Decimal
import os

import plot_mean_prevalence
import plot_peak_mean_prevalence_in_recovery
import plot_critical_infectious_period
from common import merge_namespaces, plot_and_save_using_module


def get_common_config():
    custom = types.SimpleNamespace()
    custom.infection_rate_range = numpy.arange(Decimal('1.00'), Decimal('0.0'), Decimal('-0.10'))
    custom.xlabel = 'Time t [day]'
    return custom


def mean_prevalence(customization, results_dir, parent_dir, script_name):
    module = plot_mean_prevalence
    plots_dir = os.path.join(parent_dir, module.__name__)
    path = os.path.join(plots_dir, script_name + ".png")
    customization.results_dir = results_dir

    plot_and_save_using_module(module, customization, path)


def peak_mean_prevalence(customization, results_dir, parent_dir, script_name):
    module = plot_peak_mean_prevalence_in_recovery
    plots_dir = os.path.join(parent_dir, module.__name__)
    path = os.path.join(plots_dir, script_name + ".png")
    customization.results_dir = results_dir

    plot_and_save_using_module(module, customization, path)


def critical_infection(customization, results_dir, parent_dir, script_name):
    module = plot_critical_infectious_period
    plots_dir = os.path.join(parent_dir, module.__name__)
    path = os.path.join(plots_dir, script_name + ".png")
    customization.results_dir = results_dir

    plot_and_save_using_module(module, customization, path)
