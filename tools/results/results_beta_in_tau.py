import types
import numpy
from decimal import Decimal
import os

from bootstrap import bootstrap
results_dir, parent_dir, script_name = bootstrap(__file__)

import plot_beta_in_tau
from common import plot_and_save_using_module


def get_common_config():
    custom = types.SimpleNamespace()
    custom.xlabel = 'time'
    return custom


def beta_in_tau(customization, results_dir, parent_dir, script_name):
    module = plot_beta_in_tau
    plots_dir = os.path.join(parent_dir, module.__name__)
    path = os.path.join(plots_dir, script_name + ".png")
    customization.results_dir = results_dir

    plot_and_save_using_module(module, customization, path)


def main(results_dir, parent_dir, script_name):
    custom = get_common_config()
    beta_in_tau(custom, results_dir, parent_dir, script_name)


if __name__ == '__main__':
    main(results_dir, parent_dir, script_name)
