from plot_cumulative_incidence import plot as plot_single
from plot_cumulative_incidence import load_param_space
import json
import argparse
import os
import sys


def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--plot_request", type=str, required=True, help='Path to file describing plot request')
    parser.add_argument(
        "--data_request",
        type=str,
        dest='data_requests',
        required=True,
        action='append',
        help='Paths to files describing results directory')

    args = parser.parse_args(argv)
    with open(args.plot_request) as inp:
        plot_request = json.load(inp)

    output_directory = os.path.dirname(args.plot_request)

    assert plot_request['type'] == 'plot_cumulative_incidence_sequence'

    recovery_parameter = plot_request['recovery_parameter']
    alphas = load_param_space(plot_request['alpha'])
    for recovery in load_param_space(plot_request[recovery_parameter]):
        request = {
            "version": plot_request['version'],
            "type": "plot_cumulative_incidence",
            "dataset": plot_request['dataset'],
            "recovery_parameter": recovery_parameter,
            "title": plot_request['title'],
            "defaults": plot_request['defaults'],
            "plots": [{
                "alpha": alpha,
                recovery_parameter: recovery
            } for alpha in alphas],
            "output": plot_request['output'].format(recovery=recovery)
        }
        plot_single(request, args.data_requests, output_directory)

    # pyplot.show()


if __name__ == "__main__":
    main(sys.argv[1:])
