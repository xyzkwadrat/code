from analyze import get_data_from_directory, get_sum_incidences
import os
from matplotlib import pyplot
from fstr import fstr as fstring
import types
from common import update_defaults_from_args, decimal_range


def get_defaults():
    defaults = types.SimpleNamespace()
    defaults.infection_rate = '1.00'
    defaults.recovery_rate_range = decimal_range('1.00', '0.0', '-0.1')
    defaults.label = '$\\alpha={infect}$, $\\tau={recover}$'
    defaults.xlabel = 'Time t [hour]'
    defaults.ylabel = 'Mean prevalence'
    defaults.title = 'Mean prevalence over {results.shape[1]} runs'
    defaults.fontsize = 20
    defaults.results_dir = None
    return defaults


def plot(args):
    infect = args.infection_rate
    incidences = {}

    for recover in args.recovery_rate_range:
        results_dir = os.path.join(args.results_dir, fstring("{infect}_{recover}"))

        data = get_data_from_directory(results_dir)
        sums = get_sum_incidences(data)
        incidences[recover] = max(sums)

    pyplot.scatter(list(incidences.keys()), list(incidences.values()))
    pyplot.title('SIR range for $\\alpha=1$, in function of $\\tau$')
    pyplot.xlabel('$\\tau$ [15]', fontsize=20)
    pyplot.ylabel('Range', fontsize=20)
    pyplot.grid()


if __name__ == "__main__":
    defaults = get_defaults()
    args = update_defaults_from_args(defaults)
    plot(args)
    pyplot.show()
