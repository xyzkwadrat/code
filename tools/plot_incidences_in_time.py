from datetime import datetime
from collections import OrderedDict
from matplotlib import pyplot
from common import update_defaults_from_args
import types
from fstr import fstr as fstring


def get_defaults():
    defaults = types.SimpleNamespace()
    defaults.datafile = '../data/CollegeMsg.txt'
    defaults.xlabel = 'Time t'
    defaults.ylabel = 'Number of incidences'
    defaults.title = 'Number of incidences in time'
    defaults.fontsize = 20
    return defaults


def plot(args):
    data = args.datafile

    groups = OrderedDict()
    ids = set()

    with open(data, 'r') as input:
        for line in input:
            left, right, stamp = map(int, line.split())
            ids.add(left)
            ids.add(right)

            date = datetime.fromtimestamp(stamp)
            group_id = (date.year, date.month, date.day, date.hour)
            if group_id in groups:
                groups[group_id] += 1
            else:
                groups[group_id] = 1

    pyplot.stem(range(len(groups)), groups.values())
    pyplot.title(fstring(args.title), fontsize=args.fontsize)
    pyplot.xlabel(fstring(args.xlabel), fontsize=args.fontsize)
    pyplot.ylabel(fstring(args.ylabel), fontsize=args.fontsize)
    pyplot.show()


if __name__ == "__main__":
    defaults = get_defaults()
    args = update_defaults_from_args(defaults)
    plot(args)
    pyplot.show()
