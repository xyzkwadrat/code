import numpy
from matplotlib import pyplot

if __name__ == "__main__":
    eps = numpy.float(0.05)
    beta = numpy.arange(0.01, 1, 0.01)
    tau = numpy.log(eps) / numpy.log(1 - beta)

    pyplot.semilogy(beta, tau, '.')
    pyplot.grid()
    pyplot.xlabel("$\\beta$")
    pyplot.ylabel("$\\tau$")
    pyplot.title(
        "Number of model ticks ($\\tau$) corresponding to $\\beta$ with error $\\epsilon={}$ (in 1-$\\epsilon$ trajectories)"
        .format(eps))
    pyplot.minorticks_on()
    pyplot.show()
