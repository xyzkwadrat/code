from analyze import get_data_from_directory, get_params
import os
from matplotlib import pyplot
import pandas
from decimal import Decimal
import types
from common import update_defaults_from_args, decimal_range
from fstr import fstr as fstring

#recent call: python cpp\tools\plot_permuted_mean_prevalence.py --result_dir=permuted


def get_defaults():
    defaults = types.SimpleNamespace()
    defaults.infection_rate = '0.90'
    defaults.recovery_rate = '80'
    defaults.label = '$\\alpha={infect}$, $\\tau={recover}$'
    defaults.xlabel = 'time [15\']'
    defaults.ylabel = 'Mean prevalence'
    defaults.title = 'Mean prevalence for {results.shape[1]} permutations with $\\alpha={infect}$ and $\\tau={recover}$'
    defaults.fontsize = 20
    defaults.results_dir = None
    defaults.reference_dir = None
    return defaults


def plot(args):
    infect = args.infection_rate
    recover = args.recovery_rate
    result_dir = os.path.join(args.results_dir, fstring("{infect}_{recover}"))
    data = get_data_from_directory(result_dir)
    all_plots = [data.ix[filename, 'meanPrevalence'] for filename, _ in data.groupby(level=0)]

    reference_dir = os.path.join(args.reference_dir, fstring("{infect}_{recover}"))
    reference_data = get_data_from_directory(reference_dir)
    reference_plots = [
        reference_data.ix[filename, 'meanPrevalence']
        for filename, _ in reference_data.groupby(level=0)
    ]

    results = pandas.concat(all_plots, axis=1)
    results.plot(color='blue', alpha=0.01)

    average = results.mean(axis=1)
    reference_average = pandas.concat(reference_plots, axis=1).mean(axis=1)

    ax = average.plot(color='g')
    reference_average.plot(color='r')
    minmax = pyplot.fill_between(
        results.index, results.min(axis=1), results.max(axis=1), color='grey', alpha=0.1)
    lines, _ = ax.get_legend_handles_labels()

    pyplot.legend([lines[-2], lines[-1], minmax],
                  ['Averaged plot', 'Not-permuted averaged', 'Values bounds'])

    pyplot.title(fstring(args.title), fontsize=args.fontsize)
    pyplot.xlabel(fstring(args.xlabel), fontsize=args.fontsize)
    pyplot.ylabel(fstring(args.ylabel), fontsize=args.fontsize)


if __name__ == "__main__":
    defaults = get_defaults()
    args = update_defaults_from_args(defaults)
    plot(args)
    pyplot.show()
