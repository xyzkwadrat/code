import os
import subprocess
import concurrent.futures
import numpy
import datetime
from typing import Callable
from decimal import Decimal
from common import decimal_range
import itertools
import numpy
import types


def ncycles(iterable, n):
    "Returns the sequence elements n times"
    return itertools.chain.from_iterable(itertools.repeat(tuple(iterable), n))


def guessDataFormat(file):
    file_name = os.path.split(file)[1]
    if 'pcbi.1001109.s001.csv' in file_name:
        return "SexContacts"
    elif file_name == 'CollegeMsg.txt':
        return "CollegeMsg"
    elif file_name == 'ht09_contact_list.dat':
        return "ConferenceContacts"
    else:
        raise RuntimeError("Could not guess dataformat for file {}".format(file))


def guessTimeInterval(file):
    file_name = os.path.split(file)[1]
    if 'pcbi.1001109.s001.csv' in file_name:
        return 1000, 2500
    else:
        return None, None


def call(alpha: float, beta: float, tau: int, shuffle_seed: numpy.int64, shared_args):

    outfile_base = shared_args.outfile_generator(alpha, beta, tau, shuffle_seed)
    outfile = outfile_base + '.h5'

    intermidate_dirs_mask = os.path.join(outfile_base, "{matrix}")
    since, until = guessTimeInterval(shared_args.input_file)
    flags = [
        '--sir_alpha={}'.format(alpha),
        "--sir_tau={}".format(tau) if tau else "--sir_beta={}".format(beta),
        "--shuffle_seed={}".format(shuffle_seed),
        '--input_file={}'.format(shared_args.input_file),
        "--dataformat={}".format(guessDataFormat(shared_args.input_file)),
    ]
    if since:
        flags.append("--take_since={}".format(since))
    if until:
        flags.append("--take_until={}".format(until))
    if shared_args.repetitions:
        flags.append("--repetitions={}".format(shared_args.repetitions))

    if shared_args.conference_group_by_seconds:
        flags += [
            "--conference_group_by_seconds={}".format(shared_args.conference_group_by_seconds)
        ]

    flags.append(f'--output_file={outfile}')
    command = [shared_args.app_path] + flags
    os.makedirs(os.path.dirname(outfile), exist_ok=True)

    print('Running {}'.format(' '.join(command)))
    return subprocess.call(command)


def get_parameter_space_permuted(alpha_span, tau_span, beta_span, n=50):
    shuffle_seed_span = numpy.random.randint(low=0, high=2**32, size=n, dtype=numpy.int64)
    return itertools.product(alpha_span, beta_span, tau_span, shuffle_seed_span), n


def get_parameter_space_alpha_n_reps(alpha_span, tau_span, beta_span, n=50):
    shuffle_seed_span = [0]
    print(f"Parameter space size: {len(alpha_span) * len(tau_span) * len(beta_span) * n}")
    return ncycles(itertools.product(alpha_span, beta_span, tau_span, shuffle_seed_span), n), n


def call_parameters_group(executor, parameters, shared_args):
    calls = [
        executor.submit(call, alpha, beta, tau, shuffle_seed, shared_args)
        for alpha, beta, tau, shuffle_seed in parameters
    ]
    for future in concurrent.futures.as_completed(calls):
        try:
            return_code = future.result()
        except Exception as exc:
            exit()


def files_count(directory):
    if os.path.isdir(directory):
        return len([
            name for name in os.listdir(directory) if os.path.isfile(os.path.join(directory, name))
        ])
    else:
        return 0


def options():
    import argparse
    options = argparse.ArgumentParser()
    options.add_argument('--app_path', default='./cpp/src/cmake-build-release/app/app.exe')
    options.add_argument('--result_dir', required=True)
    options.add_argument('--input_file', default='./data/ht09_contact_list.dat')
    options.add_argument('--convert_tau_to_beta', action='store_true')
    options.add_argument('--convert_tau_to_beta_eps', default=0.05)
    options.add_argument('-n', dest='dry_run', action='store_true')
    options.add_argument('--conference_group_by_seconds', type=int, default=None)
    options.add_argument('--repetitions', type=int, default=None)

    return options.parse_args()


def is_executable(path):
    return os.path.isfile(path) and os.access(path, os.X_OK)


def createSharedArgs(dry_run, input_file, app_path, outfile_generator, outdir_generator,
                     conference_group_by_seconds, repetitions):
    shared = types.SimpleNamespace()
    shared.dry_run = dry_run
    shared.input_file = input_file
    shared.app_path = app_path
    shared.outfile_generator = outfile_generator
    shared.outdir_generator = outdir_generator
    shared.conference_group_by_seconds = conference_group_by_seconds
    shared.repetitions = repetitions
    return shared


def run_concurrently(parameter_space, required_files_in_directory, shared_args, max_workers):
    with concurrent.futures.ThreadPoolExecutor(max_workers=max_workers) as executor:
        parameters_group = []
        existing_files_counts = {}
        for k, parameters in enumerate(parameter_space):
            directory = shared_args.outdir_generator(*parameters[0:3])
            if directory not in existing_files_counts:
                existing_files_counts[directory] = files_count(directory)

            if existing_files_counts[directory] < required_files_in_directory:
                if shared_args.dry_run:
                    print("Would run " + shared_args.outfile_generator(*parameters))
                else:
                    parameters_group.append(parameters)
                    existing_files_counts[directory] += 1

            if len(parameters_group) > 200:
                call_parameters_group(executor, parameters_group, shared_args)
                parameters_group.clear()

        if parameters_group:
            call_parameters_group(executor, parameters_group, shared_args)


def run_plain_for_alpha_beta_tau(alpha_span, tau_span, beta_span, required_files_in_directory,
                                 args):

    parameter_space, _ = get_parameter_space_alpha_n_reps(alpha_span, tau_span, beta_span,
                                                          required_files_in_directory)
    run_parameter_space(parameter_space, required_files_in_directory, args)


def run_permuted_for_alpha_beta_tau(alpha_span, tau_span, beta_span, required_files_in_directory,
                                    args):
    parameter_space, _ = get_parameter_space_permuted(alpha_span, tau_span, beta_span, 20)
    run_parameter_space(parameter_space, required_files_in_directory, args)


def run_parameter_space(parameter_space, required_files_in_directory, args):
    outdir_generator = lambda alpha, beta, tau: os.path.join(
        args.result_dir, '{}_{}'.format(alpha, tau if tau else beta))
    outfile_generator = lambda alpha, beta, tau, shuffle_seed: os.path.join(
        outdir_generator(alpha, beta, tau), 'd{}_p{}'.format(
            datetime.datetime.now().strftime("%d%m%Y-%H%M%S"), shuffle_seed))

    shared_args = createSharedArgs(args.dry_run, args.input_file, args.app_path, outfile_generator,
                                   outdir_generator, args.conference_group_by_seconds,
                                   args.repetitions)
    run_concurrently(parameter_space, required_files_in_directory, shared_args, max_workers=10)


def main():
    args = options()
    assert is_executable(args.app_path), f"{args.app_path} is not executable"

    alpha_span = numpy.arange(Decimal('1.00'), Decimal('0'), Decimal('-0.10'), dtype=Decimal)
    tau_span = list(decimal_range('100', '0', '-1'))

    beta_span = [None]  #list(numpy.arange(Decimal('1.00'), Decimal('0'), Decimal('-0.05'))) + \
    #list(numpy.arange(Decimal('0.04'), Decimal('0'), Decimal('-0.01')))

    if args.convert_tau_to_beta:
        from beta_in_tau import calculate_beta
        eps = Decimal(args.convert_tau_to_beta_eps)
        beta_span = [calculate_beta(float(eps), float(tau)) for tau in tau_span]
        tau_span = [None]

    run_plain_for_alpha_beta_tau(alpha_span, tau_span, beta_span, 1, args)


if __name__ == "__main__":
    main()
