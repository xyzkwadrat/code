from analyze import get_data_from_directory, get_sum_incidences
import os
from matplotlib import pyplot
import pandas
import numpy
import types
from common import update_defaults_from_args, decimal_range
from fstr import fstr as fstring


def get_defaults():
    defaults = types.SimpleNamespace()
    defaults.infection_rate_range = decimal_range("1.00", "0.00", "-0.01")
    defaults.recovery_rate = '80'
    defaults.label = '$\\alpha={infect}$, $\\tau={recover}$'
    defaults.xlabel = 'Time t [hour]'
    defaults.ylabel = 'Mean prevalence'
    defaults.title = 'Mean prevalence over {results.shape[1]} runs'
    defaults.fontsize = 20
    defaults.results_dir = None
    return defaults


def plot(args):
    recover = args.recovery_rate
    incidences = {}

    for infect in args.infection_rate_range:
        results_dir = os.path.join(args.results_dir, fstring("{infect}_{recover}"))

        data = get_data_from_directory(results_dir)
        sums = get_sum_incidences(data)
        incidences[infect] = max(sums)

    #print(incidences)
    x = sorted(incidences.keys())[:]
    y = [incidences[xx] for xx in x]
    pyplot.plot(x, y, '.')
    pyplot.show()

    x = numpy.array(sorted(incidences.keys()))
    mean = numpy.array([numpy.mean(incidences[xx]) for xx in x])
    maxed = numpy.array([numpy.max(incidences[xx]) for xx in x])

    max_of_maxed = numpy.where(maxed == maxed.max())
    print(max_of_maxed, x[max_of_maxed])

    max_of_mean = numpy.where(mean == mean.max())
    print(max_of_mean, x[max_of_mean])

    pyplot.plot(x, mean, label='Cumulative incidence averaged over 50 runs')
    pyplot.plot(x, maxed, label='Maximum of cumulative incidence from 50 runs')
    pyplot.xlabel('Transition rate $\\alpha$', fontsize=20)
    pyplot.ylabel('Cumulative incidence (% of network infected)', fontsize=20)
    pyplot.title('Cumulative incidence in function of $\\alpha$', fontsize=20)
    pyplot.text(
        x.mean(), (mean.min() + mean.max()) / 2,
        'Peak value of maximum of cumulative incidence at: $\\alpha$={} '
        '\nPeak value of averaged cumulative incidence at: $\\alpha$={}'.format(
            x[max_of_maxed][0], x[max_of_mean][0]),
        fontsize=16)
    pyplot.legend(fontsize=20)
    pyplot.grid()


if __name__ == "__main__":
    defaults = get_defaults()
    args = update_defaults_from_args(defaults)
    plot(args)
    pyplot.show()
