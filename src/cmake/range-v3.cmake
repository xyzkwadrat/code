include_guard(GLOBAL)
find_package(range-v3 REQUIRED)
set_property(TARGET range-v3 APPEND PROPERTY INTERFACE_COMPILE_OPTIONS "$<$<CXX_COMPILER_ID:MSVC>:/permissive->")
set_property(TARGET range-v3 PROPERTY IMPORTED_GLOBAL True) # to enable aliasing
add_library(external::range-v3 ALIAS range-v3)

