find_package(HDF5 REQUIRED)

include(ExternalProject)
ExternalProject_Add(HighFive
    GIT_REPOSITORY    https://github.com/BlueBrain/HighFive.git
    GIT_TAG           68fbe8e88c190f29600128e6ba4ed558f5a08cb4
    CMAKE_ARGS
              $<$<BOOL:CMAKE_TOOLCHAIN_FILE>:-DCMAKE_TOOLCHAIN_FILE:FILEPATH=${CMAKE_TOOLCHAIN_FILE}>
              -DHIGHFIVE_UNIT_TESTS=Off
              -DHIGHFIVE_EXAMPLES=Off
              -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
              -DCMAKE_INSTALL_MESSAGE=NEVER
)
ExternalProject_Get_Property(HighFive INSTALL_DIR)
execute_process(COMMAND mkdir -p ${INSTALL_DIR}/include)
add_library(HighFive::HighFive INTERFACE IMPORTED)
set_target_properties(HighFive::HighFive PROPERTIES
    INTERFACE_INCLUDE_DIRECTORIES ${INSTALL_DIR}/include
    INTERFACE_LINK_LIBRARIES hdf5::hdf5-static
)
add_dependencies(HighFive::HighFive HighFive)
