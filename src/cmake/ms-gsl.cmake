include_guard(DIRECTORY)
find_path(GSL_INCLUDE_DIR gsl/gsl)
if(GSL_INCLUDE_DIR STREQUAL "GSL_INCLUDE_DIR-NOTFOUND")
  message(FATAL_ERROR "Could not find Guidelines Support Library.")
endif()

add_library(ms::gsl INTERFACE IMPORTED)
set_target_properties(ms::gsl PROPERTIES INTERFACE_INCLUDE_DIRECTORIES ${GSL_INCLUDE_DIR})
