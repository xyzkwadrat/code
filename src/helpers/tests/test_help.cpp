#include "../include/helpers/InfiniteInputIterator.hpp"
#include "../include/helpers/SplitInputIterator.hpp"
#include <algorithm>
#include <gtest/gtest.h>
#include <iterator>
#include <range/v3/all.hpp>
#include <sstream>

using namespace testing;

TEST(InfiniteInputIterator, SimpleOperations)
{
    helpers::InfiniteInputIterator<char> infinite('P');
    ASSERT_EQ(*infinite++, 'P');
    ASSERT_EQ(*infinite++, 'P');
    ASSERT_EQ(*infinite++, 'P');
}

TEST(InfiniteInputIterator, IteratorRange)
{
    helpers::InfiniteInputIterator<char> infinite('P');
    auto rng = ranges::make_iterator_range(infinite, infinite);
    //    auto rng = ranges::iterator_range<helpers::InfiniteInputIterator<char>,
    //    helpers::InfiniteInputIterator<char>>(infinite, infinite);
    auto taken = ranges::view::take(rng, 4);
    for (auto token : taken) {
        ASSERT_EQ(token, 'P');
    }
}

constexpr bool SplitAlways(const char&)
{
    return true;
}

TEST(SplitInputIterator, InfiniteInputIteratorSplitAlways)
{
    using Input = helpers::InfiniteInputIterator<char>;
    using Sentinel = helpers::InfiniteInputIterator<char>;

    Input infinite('P');
    auto splitIterator = helpers::makeSplitInputIterator(infinite, Sentinel{}, SplitAlways);
    // auto splitIterator = helpers::SplitInputIterator<Input, Sentinel, decltype(SplitAlways)>(infinite, Sentinel{},
    // SplitAlways);
    auto list = *splitIterator++;
    ASSERT_EQ(list.size(), 0);
    for (int i = 0; i < 3; i++) {
        list = *splitIterator++;
        ASSERT_EQ(list.size(), 1);
        ASSERT_EQ(list.front(), 'P');
    }
}

TEST(StringStreamInputIterator, VerifyLogic)
{
    using Input = std::istream_iterator<char>;

    std::stringstream istream("QWE");
    Input input(istream);
    ASSERT_NE(input, Input{});

    auto token = *input++;
    ASSERT_NE(input, Input{});
    ASSERT_EQ(token, 'Q');

    token = *input++;
    ASSERT_NE(input, Input{});
    ASSERT_EQ(token, 'W');

    token = *input++;
    ASSERT_EQ(input, Input{});
    ASSERT_EQ(token, 'E');

    token = *input++;
    ASSERT_EQ(input, Input{});
    ASSERT_EQ(token, 'E');
}

TEST(SplitInputIterator, StringStreamInputIteratorSplitAlways)
{
    using Input = std::istream_iterator<char>;
    using Sentinel = Input;

    std::stringstream istream("QWE");
    Input input(istream);
    auto splitIterator = helpers::makeSplitInputIterator(input, Sentinel{}, SplitAlways);

    auto list = *splitIterator++;
    ASSERT_EQ(list.size(), 0);

    list = *splitIterator++;
    ASSERT_EQ(list.size(), 1);
    ASSERT_EQ(list.front(), 'Q');

    list = *splitIterator++;
    ASSERT_EQ(list.size(), 1);
    ASSERT_EQ(list.front(), 'W');

    list = *splitIterator++;
    ASSERT_EQ(list.size(), 1);
    ASSERT_EQ(list.front(), 'E');

    list = *splitIterator++;
    ASSERT_EQ(list.size(), 0);
}

TEST(SplitInputIterator, InfiniteInputIteratorSplitTriplets)
{
    using Input = helpers::InfiniteInputIterator<char>;
    using Sentinel = helpers::InfiniteInputIterator<char>;
    static_assert(std::is_same<Input::value_type, char>::value, "Input::value_type must be char");

    Input infinite('P');

    int counter = 0;
    auto splitter = [&counter](auto& item) { return !((counter++) % 3); };
    auto splitIterator = helpers::makeSplitInputIterator(infinite, Sentinel{}, splitter);
    auto list = *splitIterator++;
    ASSERT_EQ(list.size(), 0);
    list = *splitIterator++;
    ASSERT_EQ(list.size(), 3);
}

TEST(SplitInputIterator, StringStreamInputIteratorSplitWords)
{
    using Input = std::istreambuf_iterator<char>;
    using Sentinel = Input;

    std::stringstream istream("Few words here");
    Input input(istream);
    auto isspace = [](const char& c) { return c == ' '; };
    auto splitIterator = helpers::makeSplitInputIterator(input, Sentinel{}, isspace);

    auto list = *splitIterator++;
    ASSERT_EQ(list.size(), 3);
    ASSERT_EQ(list.front(), 'F');

    list = *splitIterator++;
    ASSERT_EQ(list.size(), 6);
    ASSERT_EQ(list.front(), ' ');

    list = *splitIterator++;
    ASSERT_EQ(list.size(), 5);
    ASSERT_EQ(list.front(), ' ');

    list = *splitIterator++;
    ASSERT_EQ(list.size(), 0);
}

TEST(SplitInputIterator, RangeOverStringStream)
{
    using Input = std::istreambuf_iterator<char>;
    using Sentinel = Input;

    std::stringstream istream("Few words here");
    Input input(istream);
    auto isspace = [](const char& c) { return c == ' '; };
    auto range = helpers::makeSplitInputRange(input, isspace);

    size_t sizes[] = {3, 6, 5};
    int k = 0;
    for (auto list : range) {
        ASSERT_EQ(list.size(), sizes[k++]);
    }
}

TEST(SplitInputIterator, TransformRangeOverStringStream)
{
    std::stringstream istream("Few words here NOTHING_INTERESTING");
    std::istreambuf_iterator<char> input(istream);
    auto range = helpers::makeSplitInputRange(input, isspace);

    std::string words[] = {"Few", " words", " here"};

    int k = 0;
    for (auto word : range | ranges::view::take(3) | ranges::view::transform([](const auto& list) {
                         return ranges::accumulate(list, std::string{});
                     })) {
        ASSERT_LE(k, 2);
        ASSERT_EQ(word, words[k++]);
    }
}
