#pragma once
#include <Eigen/Sparse>
#include <deque>
#include <range/v3/all.hpp>
#include <vector>

using Scalar = bool;
using Matrix = Eigen::SparseMatrix<Scalar>;
using Triplet = Eigen::Triplet<Scalar>;
using Triplets = std::vector<Triplet>;
