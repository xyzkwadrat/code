#pragma once
#include <boost/optional/optional.hpp>
#include <boost/predef.h>
#include <iterator>
#include <list>
#include <optional>
#include <range/v3/all.hpp>
#include <type_traits>

namespace helpers {
// CLASS TEMPLATE SplitInputIterator
template <class _IteratorIn, class _SentinelIn, class _Splitter, class _Diff = std::ptrdiff_t>
class SplitInputIterator
{
public:
    using elem_type = typename _IteratorIn::value_type;

    using iterator_category = std::input_iterator_tag;
    using value_type = std::list<elem_type>;
    using difference_type = _Diff;
    using pointer = value_type*;
    using reference = value_type&;

    constexpr SplitInputIterator(_IteratorIn input, _SentinelIn sentinel, _Splitter splitter)
        : _input(input), _sentinel(sentinel), _splitter(splitter)
    {
        _Getval();
    }

    constexpr SplitInputIterator() = default;

    constexpr SplitInputIterator(const SplitInputIterator&) = default;
    constexpr SplitInputIterator& operator=(const SplitInputIterator&) = default;

    constexpr SplitInputIterator(SplitInputIterator&&) = default;
    constexpr SplitInputIterator& operator=(SplitInputIterator&&) = default;

    const value_type& operator*() const
    { // return designated value
        return _Myval;
    }

    const value_type* operator->() const
    { // return pointer to class object
        return std::addressof(_Myval);
    }

    value_type& operator*()
    { // return designated value
        return _Myval;
    }

    value_type* operator->()
    { // return pointer to class object
        return std::addressof(_Myval);
    }

    SplitInputIterator& operator++()
    { // preincrement
        _Getval();
        return *this;
    }

    SplitInputIterator operator++(int)
    { // postincrement
        SplitInputIterator _Tmp = *this;
        std::swap(_Tmp._Myval, _Myval);
        // this->_temp = _Tmp._temp;
        _Getval();
        return (_Tmp);
    }

    bool operator==(const SplitInputIterator&) const
    { // test for iterator equality
        return _Myval.empty() && _inputEmpty();
    }

    bool operator!=(const SplitInputIterator&) const
    { // test for iterator inequality
        return !(_Myval.empty() && _inputEmpty());
    }

protected:
    constexpr bool _inputEmpty() const
    {
        return _input == _sentinel;
    }

    void _Getval()
    {
        _Myval.clear();
        if (_temp) {
            _Myval.push_back(*_temp);
            _temp.reset();
        }

        while (!_inputEmpty()) {
            _temp = *_input++;
            if (!_splitter || (*_splitter)(*_temp))
                break;
            _Myval.push_back(*_temp);
            _temp.reset();
        }
    }

    value_type _Myval{};
    _IteratorIn _input;
    _SentinelIn _sentinel;
    boost::optional<_Splitter> _splitter;
    boost::optional<elem_type> _temp;
};

template <typename _IteratorIn, typename _SentinelIn, typename _Splitter>
decltype(auto) makeSplitInputIterator(_IteratorIn&& in, _SentinelIn&& sent, _Splitter&& split)
{
    using Iterator = typename std::remove_reference_t<_IteratorIn>;
    using Sentinel = typename std::remove_reference_t<_SentinelIn>;
    return SplitInputIterator<Iterator, Sentinel, _Splitter>{
        std::forward<_IteratorIn>(in), std::forward<_SentinelIn>(sent), std::forward<_Splitter>(split)};
}

template <typename _IteratorIn, typename _Splitter>
decltype(auto) makeSplitInputIterator(_IteratorIn&& in, _Splitter&& split)
{
    using Iterator = typename std::remove_reference_t<_IteratorIn>;
    using Sentinel = Iterator;
    return SplitInputIterator<Iterator, Sentinel, _Splitter>{std::forward<_IteratorIn>(in), Sentinel{},
                                                             std::forward<_Splitter>(split)};
}

template <typename _IteratorIn, typename _Splitter>
decltype(auto) makeSplitInputRange(_IteratorIn&& in, _Splitter&& split)
{
    using InputIterator = typename std::remove_reference_t<_IteratorIn>;
    using InputSentinel = InputIterator;

    auto splitIterator =
        makeSplitInputIterator(std::forward<_IteratorIn>(in), InputSentinel{}, std::forward<_Splitter>(split));
    using SplitIterator = decltype(splitIterator);
    return ranges::make_iterator_range(splitIterator, SplitIterator{});
}

} // namespace helpers
