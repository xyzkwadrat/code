#pragma once
#include "Types.hpp"

namespace helpers {
inline Matrix andNot(const Matrix& left, const Matrix& right)
{
    Matrix temp = left;
    temp.prune([right](const auto& row, const auto& col, const auto& value) { return !right.coeff(row, col); });
    return temp;
}

inline Triplet mirrored(const Triplet& base)
{
    return {base.col(), base.row(), base.value()};
}

template <typename TripletCollection>
Matrix tripletCollectionToSymmetricMatrix(const TripletCollection& tripletCollection, std::size_t size)
{
    Triplets symmetric = ranges::view::concat(tripletCollection, ranges::view::transform(tripletCollection, mirrored));
    Matrix mat(size, size);
    mat.setFromTriplets(symmetric.begin(), symmetric.end());
    return std::move(mat);
};
} // namespace helpers
