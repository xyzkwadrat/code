#include <iterator>
#include <type_traits>

namespace helpers {
// CLASS TEMPLATE InfiniteInputIterator
template <class _Ty, class _Diff = std::ptrdiff_t>
class InfiniteInputIterator
{
public:
    using iterator_category = std::input_iterator_tag;
    using value_type = _Ty;
    using difference_type = _Diff;
    using pointer = const _Ty*;
    using reference = const _Ty&;

    constexpr InfiniteInputIterator(_Ty&& value) : _Myval(value)
    { // construct singular iterator
    }

    constexpr InfiniteInputIterator() = default;

    constexpr InfiniteInputIterator(const InfiniteInputIterator&) = default;
    constexpr InfiniteInputIterator& operator=(const InfiniteInputIterator&) = default;

    const _Ty& operator*() const
    { // return designated value
        return _Myval;
    }

    const _Ty* operator->() const
    { // return pointer to class object
        return std::addressof(_Myval);
    }

    InfiniteInputIterator& operator++()
    { // preincrement
        _Getval();
        return (*this);
    }

    InfiniteInputIterator operator++(int)
    { // postincrement
        InfiniteInputIterator _Tmp = *this;
        ++*this;
        return (_Tmp);
    }

    bool operator==(const InfiniteInputIterator&) const
    { // test for iterator equality
        return false;
    }

    bool operator!=(const InfiniteInputIterator&) const
    { // test for iterator inequality
        return true;
    }

protected:
    void _Getval()
    { // noops
    }
    _Ty _Myval{};
};

} // namespace helpers
