add_library(app-lib
    src/SIR.hpp
    src/TauSIR.cpp
    src/TauSIR.hpp
    src/BetaSIR.cpp
    src/BetaSIR.hpp
    src/AlphaTauSIR.cpp
    src/AlphaTauSIR.hpp
    src/AlphaBetaSIR.cpp
    src/AlphaBetaSIR.hpp
)

target_link_libraries(app-lib
    PUBLIC
        Eigen3::Eigen
        external::range-v3
        helpers
    PRIVATE
        ms::gsl
)

add_executable(app
    src/app.cpp
)

target_link_libraries(app PUBLIC
    app-lib
    loader
    Boost::program_options
    fmt::fmt
    HighFive::HighFive
    Boost::boost
)

target_compile_definitions(app PRIVATE
    BOOST_CONFIG_SUPPRESS_OUTDATED_MESSAGE
    H5_USE_BOOST
)

if(BUILD_TESTS)
    add_subdirectory(tests)
endif()
