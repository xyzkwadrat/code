add_executable(testsir
    test.cpp
)
target_link_libraries(testsir PRIVATE
    GTest::GTest
    GTest::Main
    app-lib
)

add_test(testsir testsir)

