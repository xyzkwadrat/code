#include "../src/TauSIR.hpp"
#include "helpers/Operations.hpp"
#include <gtest/gtest.h>
#include <sstream>

using namespace testing;

namespace {
Matrix getIdentityMatrix(int size)
{
    Matrix selfLoop(size, size);
    selfLoop.setIdentity();
    return selfLoop;
}

Matrix getZeroMatrix(int size)
{
    Matrix mat(size, size);
    mat.setZero();
    return mat;
}

Matrix getOnesMatrix(int size)
{
    Matrix mat(size, size);
    mat.reserve(mat.size());
    for (int row = 0; row < size; row++)
        for (int col = 0; col < size; col++)
            mat.coeffRef(row, col) = true;
    return mat;
}

} // namespace
namespace Eigen {
bool operator==(const ::Matrix& left, const ::Matrix& right)
{
    return left.toDense() == right.toDense();
    if (left.cols() != right.cols() || left.rows() != right.rows())
        return false;

    for (int row = 0; row < left.rows(); row++)
        for (int col = 0; col < left.cols(); col++)
            if (left.coeff(row, col) != right.coeff(row, col))
                return false;
    return true;
}

} // namespace Eigen

TEST(DeterministicSIRTest, SinglePerson)
{
    TauSIR singlePerson(2, 1);
    ASSERT_TRUE(singlePerson.prevalenceMatrix().coeff(0, 0));
    ASSERT_FALSE(singlePerson.recoveredMatrix().coeff(0, 0));

    Triplets selfLoop = {{0, 0, true}};
    singlePerson.tick(selfLoop);
    ASSERT_TRUE(singlePerson.prevalenceMatrix().coeff(0, 0));
    ASSERT_FALSE(singlePerson.recoveredMatrix().coeff(0, 0));

    singlePerson.tick(selfLoop);
    ASSERT_FALSE(singlePerson.prevalenceMatrix().coeff(0, 0));
    ASSERT_TRUE(singlePerson.recoveredMatrix().coeff(0, 0));
}

TEST(DeterministicSIRTest, TwoPersons)
{
    TauSIR model(1, 2);
    ASSERT_EQ(model.prevalenceMatrix(), getIdentityMatrix(2));
    ASSERT_EQ(model.recoveredMatrix(), getZeroMatrix(2));

    Triplets connected;
    connected.emplace_back(1, 0, true);
    connected.emplace_back(0, 1, true);
    auto connectedMat = helpers::tripletCollectionToSymmetricMatrix(connected, 2);

    model.tick(connected);
    ASSERT_EQ(model.prevalenceMatrix(), connectedMat);
    ASSERT_EQ(model.recoveredMatrix(), getIdentityMatrix(2));

    Triplets noConnection{};
    model.tick(noConnection);
    ASSERT_EQ(model.prevalenceMatrix(), getZeroMatrix(2));
    ASSERT_EQ(model.recoveredMatrix(), getOnesMatrix(2));
}

TEST(DeterministicSIRTest, TwoPerson2)
{
    Eigen::SparseMatrix<int, 0, int> aaa{5, 5};
    aaa.setIdentity();
    aaa.coeffRef(1, 2) = 30;
    Eigen::Matrix2i qwe;
    Eigen::Matrix<bool, 3, 5> abc;
    abc.coeffRef(0, 0) = abc.coeffRef(0, 3) = 0;

    Triplets connected;
    connected.emplace_back(1, 0, true);
    auto connectedMat = helpers::tripletCollectionToSymmetricMatrix(connected, 2);

    TauSIR model(1, 2);
    model.tick(connected);
    ASSERT_EQ(model.prevalenceMatrix(), connectedMat);
    ASSERT_EQ(model.recoveredMatrix(), getIdentityMatrix(2));

    Triplets noConnection{};
    model.tick(noConnection);
    ASSERT_EQ(model.prevalenceMatrix(), getZeroMatrix(2));
    ASSERT_EQ(model.recoveredMatrix(), connectedMat || getIdentityMatrix(2));
}
