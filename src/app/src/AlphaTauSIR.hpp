#pragma once
#include "SIR.hpp"
#include "TauSIR.hpp"
#include "helpers/Types.hpp"
#include <random>

class AlphaTauSIR final : public SIR
{
public:
    AlphaTauSIR(int tau, Eigen::Index size, float alpha, unsigned int seed);

    void tick(const Triplets&) override;

    const Matrix& prevalenceMatrix() const override;
    const Matrix& recoveredMatrix() const override;
    const Matrix& incidenceMatrix() const override;
    std::string name() const override;

private:
    TauSIR deterministicRunner;
    float alpha;
    std::mt19937 generator;
    std::uniform_real_distribution<float> distribution{0, 1.0f};
};
