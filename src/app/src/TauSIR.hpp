#pragma once
#include "SIR.hpp"
#include "helpers/Types.hpp"

class TauSIR final : public SIR
{
public:
    TauSIR(int tau, Eigen::Index size);

    void tick(const Triplets&) override;

    const Matrix& prevalenceMatrix() const override;
    const Matrix& recoveredMatrix() const override;
    const Matrix& incidenceMatrix() const override;
    std::string name() const override;

private:
    Matrix I;
    Matrix R;
    Matrix J;
    std::deque<Matrix> historicI;
    int tau;
};
