//
// Created by R2 on 5/26/2018.
//

#pragma once

#ifdef __MINGW32__
#include <windows.h>
inline uint32_t get_random_seed()
{
    HMODULE hLib = LoadLibrary("ADVAPI32.DLL");
    uint32_t seed{};
    if (hLib) {
        BOOLEAN(APIENTRY * pfn)
        (void*, ULONG) = (BOOLEAN(APIENTRY*)(void*, ULONG))GetProcAddress(hLib, "SystemFunction036");
        if (pfn) {
            if (!pfn(&seed, sizeof(seed))) {
                FreeLibrary(hLib);
                throw std::runtime_error("Failed to generate random seed");
            }
        }

        FreeLibrary(hLib);
    }
    return seed;
}
#else
#include <random>
inline uint32_t get_random_seed()
{
    std::random_device rd{};
    return rd();
}

#endif
