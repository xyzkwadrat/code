#include "AlphaTauSIR.hpp"
#include <gsl/gsl_assert>

AlphaTauSIR::AlphaTauSIR(int tau, Eigen::Index size, float alpha, unsigned int seed)
    : deterministicRunner{tau, size}, alpha(alpha), generator{seed}
{
    Ensures(alpha >= 0 && alpha <= 1);
}

const Matrix& AlphaTauSIR::prevalenceMatrix() const
{
    return deterministicRunner.prevalenceMatrix();
}

const Matrix& AlphaTauSIR::recoveredMatrix() const
{
    return deterministicRunner.recoveredMatrix();
}

const Matrix& AlphaTauSIR::incidenceMatrix() const
{
    return deterministicRunner.incidenceMatrix();
}

void AlphaTauSIR::tick(const Triplets& collection)
{
    Triplets filtered;
    filtered.reserve(collection.size() * alpha);
    for (const auto& triplet : collection) {
        if (distribution(generator) <= alpha) {
            filtered.emplace_back(triplet);
        }
    }

    deterministicRunner.tick(filtered);
}

std::string AlphaTauSIR::name() const
{
    return "AlphaTauSIR";
};
