#include "BetaSIR.hpp"
#include "helpers/Operations.hpp"
#include <gsl/gsl_assert>

BetaSIR::BetaSIR(float beta, Eigen::Index size, unsigned int seed)
    : I{size, size}, R{size, size}, beta(beta), J{size, size}, generator{seed}, distribution{0, 1.0f}
{
    Ensures(beta >= 0.0f && beta <= 1.0f);
    I.setIdentity();
    R.setZero();
    J.setZero();
}

const Matrix& BetaSIR::prevalenceMatrix() const
{
    return I;
}

const Matrix& BetaSIR::recoveredMatrix() const
{
    return R;
}

const Matrix& BetaSIR::incidenceMatrix() const
{
    return J;
}

void BetaSIR::tick(const Triplets& collection)
{
    auto A = helpers::tripletCollectionToSymmetricMatrix(collection, I.cols());

    auto betaTimesI = I;
    betaTimesI.prune([this](const auto&, const auto&, const auto&) { return distribution(generator) <= beta; });

    R = R || betaTimesI;

    Matrix nextInfectedMatrix = helpers::andNot(A * I || I, R);
    J = helpers::andNot(nextInfectedMatrix, I);
    I = nextInfectedMatrix;
}

std::string BetaSIR::name() const
{
    return "BetaSIR";
};
