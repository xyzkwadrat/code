#include "AlphaBetaSIR.hpp"
#include "AlphaTauSIR.hpp"
#include "BetaSIR.hpp"
#include "TauSIR.hpp"
#include "random_seed.hpp"
#include <boost/program_options.hpp>
#include <fmt/format.h>
#include <fstream>
#include <iomanip>
#include <iostream>
#include <limits>
#include <loader/CollegeMsgLoader.hpp>
#include <loader/ConferenceContactsLoader.hpp>
#include <loader/SexContactsLoader.hpp>
#include <map>
#include <memory>
#include <range/v3/algorithm/shuffle.hpp>
#include <string>

#include <highfive/H5DataSet.hpp>
#include <highfive/H5DataSpace.hpp>
#include <highfive/H5File.hpp>
#include <highfive/H5Group.hpp>
#include <highfive/H5PropertyList.hpp>
#include <highfive/H5Utility.hpp>

#include <highfive/H5DataSet.hpp>
#include <highfive/H5DataSpace.hpp>
#include <highfive/H5File.hpp>

auto safe_slice(std::size_t since, std::size_t until)
{
    return ranges::view::drop(since) | ranges::view::take(until - since);
}

using IdMap = std::map<int, int>;

IdMap createIdMap(const std::vector<Triplets>& tripletsCollection, std::size_t since, std::size_t until)
{
    IdMap map;
    for (const auto& triplets : tripletsCollection | safe_slice(since, until)) {
        for (const auto& triplet : triplets) {
            map.insert(std::make_pair(triplet.row(), map.size()));
            map.insert(std::make_pair(triplet.col(), map.size()));
        }
    }
    return std::move(map);
};

void skipComments(std::ifstream& input)
{
    while (input.good() && input.peek() == '#') {
        std::string tmp;
        std::getline(input, tmp);
    }
}

enum class DataFormat
{
    SexContacts,
    ConferenceContacts,
    CollegeMsg
};

std::istream& operator>>(std::istream& input, DataFormat& dataFormat)
{
    std::string name;
    input >> name;
    if (name == "SexContacts") {
        dataFormat = DataFormat::SexContacts;
    } else if (name == "ConferenceContacts") {
        dataFormat = DataFormat::ConferenceContacts;
    } else if (name == "CollegeMsg") {
        dataFormat = DataFormat::CollegeMsg;
    } else {
        input.setstate(std::ios_base::failbit);
    }
    return input;
}

std::vector<Triplets> loadData(std::istream& input, DataFormat dataFormat,
                               const std::string& collegeMsgGroupingTimeFormat, std::uint64_t conferenceGroupBySeconds)
{
    switch (dataFormat) {
    case DataFormat::CollegeMsg:
        return loader::CollegeMsgLoader::getTripletsGroupedByTimeMask(input, collegeMsgGroupingTimeFormat);
    case DataFormat::ConferenceContacts:
        return loader::ConferenceContactsLoader::getTripletsGroupedBySeconds(input, conferenceGroupBySeconds);
    case DataFormat::SexContacts:
        return loader::SexContact::getTripletsPerDay(input);
    }
    throw std::runtime_error("Data loader for " + std::to_string(static_cast<int>(dataFormat)) + " is not implemented");
}

struct Arguments
{
    std::string inputFile;
    std::string outputFile;
    std::uint32_t repetitions;
    DataFormat dataFormat;
    boost::optional<std::uint8_t> compressionLevel;
    float alpha;
    std::uint32_t tau;
    float beta;
    boost::optional<std::uint32_t> shuffleSeed;
    std::uint8_t precision;
    std::string collegeMsgGroupingTimeFormat;
    std::uint32_t takeSince;
    boost::optional<std::uint32_t> takeUntil;
    std::uint32_t conferenceGroupBySeconds;
};

bool parseArgs(Arguments& args, int argc, char** argv)
{
    namespace po = boost::program_options;
    po::options_description required("Required options");
    // clang-format off
    required.add_options()("help", "Produce this help message")
        ("input_file", po::value<std::string>(&args.inputFile)->required(), "Path to input file")
        ("output_file", po::value<std::string>(&args.outputFile)->required(), "Path to output file")
        ("dataformat", po::value<DataFormat>(&args.dataFormat)->required(), "Which dataformat loader should be used. "
            "Possible values: CollegeMsg, ConferenceContacts, SexContacts")
        ("sir_alpha", po::value<float>(&args.alpha)->required(), "Chances to distribute disease [0,1]")
        ("sir_tau", po::value<std::uint32_t>(&args.tau), "Time for I -> R. Only tau or beta should be specified")
        ("sir_beta", po::value<float>(&args.beta), "Changes for I -> R. Only tau or beta should be specified")
        ("compression", po::value<boost::optional<std::uint8_t>>(&args.compressionLevel), "Set HDF5's deflate compression level. No compression if not passed.")
        ("repetitions", po::value<std::uint32_t>(&args.repetitions)->default_value(1), "Number of simulation runs to be stored in single file.")
        ("college_msg_grouping_time_format", po::value<std::string>(&args.collegeMsgGroupingTimeFormat)->default_value("%Y%m%d%H"),
             "Time format used for grouping of messages (sprintf syntax)")
        ("out_precision", po::value<std::uint8_t>(&args.precision)->default_value(13u), "Precision used for output values")
        ("shuffle_seed", po::value<boost::optional<std::uint32_t>>(&args.shuffleSeed), "Optinally shuffle applied for mixing order of triplets, "
                                                     "bases on std::shuffle")
        ("take_since", po::value<std::uint32_t>(&args.takeSince)->default_value(0), "Lower bound of time interval of simulation")
        ("take_until", po::value<boost::optional<std::uint32_t>>(&args.takeUntil), "Time to end simulation prematurely. -1 means no bound")
        ("conference_group_by_seconds", po::value<std::uint32_t>(&args.conferenceGroupBySeconds)->default_value(900),
                                                "Divisor used for grouping of contacts. "
                                                "Events with same time after division are considered to "
                                                "happen simultaneously (used in same matrix)")
        ;
    // clang-format on
    po::options_description all("Allowed options");
    all.add(required);

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, all), vm);

    if (vm.count("help")) {
        std::cout << all << "\n";
        return false;
    }
    try {
        po::notify(vm);
        return true;
    } catch (const boost::program_options::required_option& ex) {
        std::cout << all << "\nMissing required option: " << ex.get_option_name() << "\nSee help message above\n";
        return false;
    }
}

std::vector<Triplets> loadData(const Arguments& args)
{
    std::ifstream input(args.inputFile);
    skipComments(input);
    return loadData(input, args.dataFormat, args.collegeMsgGroupingTimeFormat, args.conferenceGroupBySeconds);
}

struct SimulationRun
{
    std::vector<float> prevelances;
    std::vector<float> incidences;
    std::vector<float> cumulativeIncidence;
    std::string algorithm;
    std::uint32_t seed;
    std::uint32_t since;
    std::uint32_t until;
};

SimulationRun runOnce(const std::vector<Triplets>& tripletsPerDay, const std::uint32_t since, const std::uint32_t until,
                      const float alpha, const float beta, const uint32_t tau,
                      const boost::optional<std::uint32_t>& shuffleSeed)
{
    Ensures(since < until);

    auto idMap = createIdMap(tripletsPerDay, since, until);

    auto remapTriplets = [&](const Triplets& triplets) -> Triplets {
        return ranges::view::transform(triplets, [&](const Triplet& triplet) {
            return Triplet{idMap[triplet.row()], idMap[triplet.col()], triplet.value()};
        });
    };

    uint32_t seed = get_random_seed();
    auto sir = [&]() -> std::unique_ptr<SIR> {
        Ensures(!(beta > 0 && tau > 0)); // Only one of tau and beta can be non-zero

        if (tau != 0) {
            if (alpha < 1) {
                return std::make_unique<AlphaTauSIR>(tau, idMap.size(), alpha, seed);
            } else {
                return std::make_unique<TauSIR>(tau, idMap.size());
            }
        } else {
            if (alpha < 1) {
                return std::make_unique<AlphaBetaSIR>(alpha, beta, idMap.size(), seed);
            } else {
                return std::make_unique<BetaSIR>(beta, idMap.size(), seed);
            }
        }
    }();

    std::vector<Triplets> remappedTripletsPerDay =
        tripletsPerDay | safe_slice(since, until) | ranges::view::transform(remapTriplets);

    if (shuffleSeed) {
        std::mt19937 generator{*shuffleSeed};
        ranges::shuffle(remappedTripletsPerDay, generator);
    }

    const auto eps = 1e-14;
    std::vector<float> prevelances;
    prevelances.reserve(remappedTripletsPerDay.size());

    std::vector<float> incidences;
    incidences.reserve(remappedTripletsPerDay.size());

    std::vector<float> cumulativeIncidence;
    cumulativeIncidence.reserve(remappedTripletsPerDay.size());

    for (auto triplets : remappedTripletsPerDay) {
        sir->tick(triplets);
        prevelances.emplace_back(sir->meanPrevalence());
        auto inc = sir->meanIncidence();
        incidences.emplace_back(inc);
        cumulativeIncidence.emplace_back(cumulativeIncidence.empty() ? inc : cumulativeIncidence.back() + inc);
        if (std::abs(sir->meanPrevalence()) < eps && std::abs(sir->meanIncidence()) < eps)
            break;
    }
    return {
        std::move(prevelances), std::move(incidences), std::move(cumulativeIncidence), sir->name(), seed, since, until,
    };
}

int main(int argc, char** argv)
{
    Arguments args{};
    if (!parseArgs(args, argc, argv)) {
        return 1;
    }

    const auto tripletsPerDay = loadData(args);

    const auto since = args.takeSince;
    const auto until = args.takeUntil.get_value_or(tripletsPerDay.size());
    const std::uint32_t repetitions = args.repetitions;

    boost::multi_array<float, 3> data(boost::extents[3][tripletsPerDay.size()][repetitions]);
    boost::multi_array<std::uint32_t, 1> seeds(boost::extents[repetitions]);

    std::string algorithm;

    for (std::uint32_t k = 0; k < repetitions; k++) {
        auto run = runOnce(tripletsPerDay, since, until, args.alpha, args.beta, args.tau, args.shuffleSeed);

        for (std::size_t i = 0; i < run.prevelances.size(); i++) {
            data[0][i][k] = run.prevelances[i];
            data[1][i][k] = run.incidences[i];
            data[2][i][k] = run.cumulativeIncidence[i];
        }
        for (std::size_t i = run.prevelances.size(); i < tripletsPerDay.size(); i++) {
            data[0][i][k] = 0;
            data[1][i][k] = 0;
            data[2][i][k] = run.cumulativeIncidence.back();
        }

        if (algorithm.empty()) {
            algorithm = std::move(run.algorithm);
        }
        seeds[k] = run.seed;
    }

    try {
        HighFive::File file(args.outputFile,
                            HighFive::File::ReadWrite | HighFive::File::Create | HighFive::File::Truncate);

        // Use chunking
        HighFive::DataSetCreateProps props;
        // Enable deflate
        if (args.compressionLevel) {
            props.add(HighFive::Chunking(std::vector<hsize_t>{1, tripletsPerDay.size(), repetitions}));
            props.add(HighFive::Deflate(*args.compressionLevel));
        }

        HighFive::DataSet dataset = file.createDataSet<float>("data", HighFive::DataSpace::From(data), props);

        HighFive::Attribute aAlg =
            dataset.createAttribute<std::string>("algorithm", HighFive::DataSpace::From(algorithm));
        aAlg.write(algorithm);

        HighFive::Attribute aTau = dataset.createAttribute<std::int32_t>("tau", HighFive::DataSpace::From(args.tau));
        aTau.write(args.tau);

        HighFive::Attribute aAlpha = dataset.createAttribute<float>("alpha", HighFive::DataSpace::From(args.alpha));
        aAlpha.write(args.alpha);

        HighFive::Attribute aBeta = dataset.createAttribute<float>("beta", HighFive::DataSpace::From(args.beta));
        aBeta.write(args.beta);

        std::uint32_t size = tripletsPerDay.size();
        HighFive::Attribute aSize = dataset.createAttribute<std::uint32_t>("size", HighFive::DataSpace::From(size));
        aSize.write(size);

        HighFive::Attribute aSince = dataset.createAttribute<std::uint32_t>("since", HighFive::DataSpace::From(since));
        aSince.write(since);

        HighFive::Attribute aUntil = dataset.createAttribute<std::uint32_t>("until", HighFive::DataSpace::From(until));
        aUntil.write(until);

        dataset.write(data);

        HighFive::DataSet seedsDataset = file.createDataSet<std::uint32_t>("seeds", HighFive::DataSpace::From(seeds));
        seedsDataset.write(seeds);
    } catch (HighFive::Exception& err) {
        // catch and print any HDF5 error
        std::cerr << err.what() << std::endl;
        return 2;
    }
    return 0;
}
