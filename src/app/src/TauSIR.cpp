#include "TauSIR.hpp"
#include "helpers/Operations.hpp"
#include <gsl/gsl_assert>

TauSIR::TauSIR(int tau, Eigen::Index size) : I{size, size}, R{size, size}, tau(tau), J{size, size}
{
    Ensures(tau > 0);
    I.setIdentity();
    R.setZero();
    J.setZero();
    historicI.push_back(I);
}

const Matrix& TauSIR::prevalenceMatrix() const
{
    return I;
}

const Matrix& TauSIR::recoveredMatrix() const
{
    return R;
}

const Matrix& TauSIR::incidenceMatrix() const
{
    return J;
}

void TauSIR::tick(const Triplets& collection)
{
    auto A = helpers::tripletCollectionToSymmetricMatrix(collection, I.cols());
    if (historicI.size() >= tau) {
        R = R || historicI.front();
        historicI.pop_front();
    }

    Matrix nextInfectedMatrix = helpers::andNot(A * I || I, R);
    J = helpers::andNot(nextInfectedMatrix, I);
    I = nextInfectedMatrix;
    historicI.push_back(I);
}

std::string TauSIR::name() const
{
    return "TauSIR";
};
