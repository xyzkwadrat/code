#include "AlphaBetaSIR.hpp"
#include "helpers/Operations.hpp"
#include <gsl/gsl_assert>

AlphaBetaSIR::AlphaBetaSIR(float alpha, float beta, Eigen::Index size, unsigned int seed)
    : I{size, size}, R{size, size}, alpha(alpha), beta(beta), J{size, size}, generator{seed}, distribution{0, 1.0f}
{
    Ensures(alpha >= 0.0f && alpha <= 1.0f);
    Ensures(beta >= 0.0f && beta <= 1.0f);
    I.setIdentity();
    R.setZero();
    J.setZero();
}

const Matrix& AlphaBetaSIR::prevalenceMatrix() const
{
    return I;
}

const Matrix& AlphaBetaSIR::recoveredMatrix() const
{
    return R;
}

const Matrix& AlphaBetaSIR::incidenceMatrix() const
{
    return J;
}

void AlphaBetaSIR::tick(const Triplets& collection)
{
    Triplets filtered;
    filtered.reserve(collection.size() * alpha);
    for (const auto& triplet : collection) {
        if (distribution(generator) <= alpha) {
            filtered.emplace_back(triplet);
        }
    }
    auto A = helpers::tripletCollectionToSymmetricMatrix(filtered, I.cols());

    auto betaTimesI = I;
    betaTimesI.prune([this](const auto&, const auto&, const auto&) { return distribution(generator) <= beta; });

    R = R || betaTimesI;

    Matrix nextInfectedMatrix = helpers::andNot(A * I || I, R);
    J = helpers::andNot(nextInfectedMatrix, I);
    I = nextInfectedMatrix;
}

std::string AlphaBetaSIR::name() const
{
    return "AlphaBetaSIR";
};
