#pragma once
#include "SIR.hpp"
#include "helpers/Types.hpp"

class BetaSIR final : public SIR
{
public:
    BetaSIR(float beta, Eigen::Index size, unsigned int seed);

    void tick(const Triplets&) override;

    const Matrix& prevalenceMatrix() const override;
    const Matrix& recoveredMatrix() const override;
    const Matrix& incidenceMatrix() const override;
    std::string name() const override;

private:
    Matrix I;
    Matrix R;
    Matrix J;
    float beta;
    std::mt19937 generator;
    std::uniform_real_distribution<float> distribution;
};
