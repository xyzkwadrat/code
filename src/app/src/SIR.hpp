#pragma once
#include "helpers/Types.hpp"

class SIR
{
public:
    virtual void tick(const Triplets&) = 0;
    virtual const Matrix& prevalenceMatrix() const = 0;
    virtual const Matrix& recoveredMatrix() const = 0;
    virtual const Matrix& incidenceMatrix() const = 0;
    virtual std::string name() const = 0;

    inline float meanPrevalence() const
    {
        const auto& I = prevalenceMatrix();
        return static_cast<float>(I.nonZeros()) / I.size();
    }

    inline float meanIncidence() const
    {
        const auto& I = incidenceMatrix();
        return static_cast<float>(I.nonZeros()) / I.size();
    }
    virtual ~SIR() = default;
};
