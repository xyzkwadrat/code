#include <fmt/format.h>
#include <loader/ConferenceContactsLoader.hpp>

namespace loader {
namespace ConferenceContactsLoader {

std::istream& operator>>(std::istream& istream, Entry& entry)
{
    istream >> entry.time >> entry.first >> entry.second;
    // istream.setstate(std::io s::failbit);
    return istream;
}
} // namespace ConferenceContactsLoader
} // namespace loader
