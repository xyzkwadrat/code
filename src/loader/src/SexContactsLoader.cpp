#include <fmt/format.h>
#include <loader/SexContactsLoader.hpp>

namespace loader {
namespace SexContact {
namespace {
std::istream& operator>>(std::istream& istream, Grade& rhs)
{
    if (istream.eof() || istream.bad())
        return istream;

    int value;
    istream >> value;
    if (value == static_cast<int>(Grade::Bad))
        rhs = Grade::Bad;
    else if (value == static_cast<int>(Grade::Good))
        rhs = Grade::Good;
    else if (value == static_cast<int>(Grade::Neutral))
        rhs = Grade::Neutral;
    else
        throw std::runtime_error(fmt::format("{} is not valid value for Grade", value));
    return istream;
}

std::istream& operator>>(std::istream& istream, FuzzyBool& rhs)
{
    if (istream.eof() || istream.bad())
        return istream;

    int value;
    istream >> value;
    if (value == static_cast<int>(FuzzyBool::Yes))
        rhs = FuzzyBool::Yes;
    else if (value == static_cast<int>(FuzzyBool::No))
        rhs = FuzzyBool::No;
    else if (value == static_cast<int>(FuzzyBool::NoInfo))
        rhs = FuzzyBool::NoInfo;
    else
        throw std::runtime_error(fmt::format("{} is not valid value for FuzzyBool", value));
    return istream;
}
} // namespace

std::istream& operator>>(std::istream& istream, Entry& entry)
{
    char delim;
    istream >> entry.femaleId >> delim;
    istream >> entry.maleId >> delim;
    istream >> entry.day >> delim;
    istream >> entry.femaleGrade >> delim;
    istream >> entry.contactOne >> delim;
    istream >> entry.contactTwo >> delim;
    istream >> entry.contactThree;
    // istream.setstate(std::ios::failbit);
    return istream;
}
} // namespace SexContact
} // namespace loader
