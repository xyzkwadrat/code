#include <fmt/format.h>
#include <loader/CollegeMsgLoader.hpp>

namespace loader {
namespace CollegeMsgLoader {

std::istream& operator>>(std::istream& istream, Entry& entry)
{
    istream >> entry.first >> entry.second >> entry.time;
    // istream.setstate(std::io s::failbit);
    return istream;
}
} // namespace CollegeMsgLoader
} // namespace loader
