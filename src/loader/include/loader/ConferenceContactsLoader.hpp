#include <boost/fusion/adapted/boost_tuple.hpp>
#include <boost/fusion/adapted/struct/adapt_struct.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/algorithm.hpp>
#include <boost/fusion/include/boost_tuple.hpp>
#include <boost/fusion/include/sequence.hpp>
#include <gsl/gsl_util>
#include <helpers/SplitInputIterator.hpp>
#include <helpers/Types.hpp>
#include <istream>
#include <list>
#include <range/v3/all.hpp>

namespace loader {
namespace ConferenceContactsLoader {
struct Entry
{
    uint64_t time{};
    int first{};
    int second{};
};

std::istream& operator>>(std::istream& istream, Entry& entry);

constexpr bool operator==(const Entry& lhs, const Entry& rhs);

constexpr bool operator!=(const Entry& lhs, const Entry& rhs)
{
    return !(lhs == rhs);
}

inline auto getEntriesRange(std::istream& input)
{
    return ranges::iterator_range<std::istream_iterator<Entry>>(input, std::istream_iterator<Entry>{});
}

struct GroupBySeconds
{
    int previousValue = -1;
    uint64_t seconds{};
    constexpr GroupBySeconds(uint64_t seconds) : seconds(seconds) {}
    constexpr GroupBySeconds(const GroupBySeconds& other) = default;
    constexpr GroupBySeconds(GroupBySeconds&& other) = default;
    GroupBySeconds& operator=(const GroupBySeconds& other) = default;
    GroupBySeconds& operator=(GroupBySeconds&& other) = default;

    uint64_t groupedByTime(uint64_t time)
    {
        return time / seconds;
    }

    bool operator()(Entry& entry) noexcept
    {
        auto value = groupedByTime(entry.time);
        bool ret = previousValue != -1 && previousValue != value;
        previousValue = value;
        return ret;
    }
};

inline Triplet entryToTriplet(const Entry& entry)
{
    return Triplet{entry.first, entry.second, true};
};

inline auto entryListToTriplets(std::list<Entry>& list)
{
    Triplets out{};
    out.reserve(ranges::distance(list));
    uint64_t time;
    for (auto& entry : list) {
        auto triplet = entryToTriplet(entry);
        out.emplace_back(triplet);
        time = list.front().time;
    }
    return std::make_pair(time, out);
};

inline auto getEntriesRangeGroupedBySeconds(std::istream& input, uint64_t seconds)
{
    return helpers::makeSplitInputRange(std::istream_iterator<Entry>{input}, GroupBySeconds{seconds});
}

inline auto getTripletsGroupedBySeconds(std::istream& input, uint64_t seconds)
{
    auto entriesListPerDay = getEntriesRangeGroupedBySeconds(input, seconds);
    auto tripletsListPerDay = entriesListPerDay | ranges::view::transform(entryListToTriplets);

    constexpr auto maxDays = 220000;
    const auto tripletsPerDay = [&] {
        std::vector<Triplets> tripletsPerDay(maxDays / seconds);
        int lastDay = 0;

        for (auto pair : tripletsListPerDay) {
            auto& [day, triplets] = pair;
            auto group = day / seconds;
            tripletsPerDay[group] = std::move(triplets);
            lastDay = group;

            if (day >= maxDays) // sad replacement of take_while :(
                break;
        }
        tripletsPerDay.resize(lastDay + 1);
        return std::move(tripletsPerDay);
    }();

    return tripletsPerDay;
}

} // namespace ConferenceContactsLoader

} // namespace loader

BOOST_FUSION_ADAPT_STRUCT(loader::ConferenceContactsLoader::Entry, time, first, second);

constexpr bool loader::ConferenceContactsLoader::operator==(const Entry& lhs, const Entry& rhs)
{
    using namespace boost::fusion;
    return equal_to(lhs, rhs);
}
