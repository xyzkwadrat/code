#include <boost/fusion/adapted/boost_tuple.hpp>
#include <boost/fusion/adapted/struct/adapt_struct.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/algorithm.hpp>
#include <boost/fusion/include/boost_tuple.hpp>
#include <boost/fusion/include/sequence.hpp>
#include <ctime>
#include <gsl/gsl_util>
#include <helpers/SplitInputIterator.hpp>
#include <helpers/Types.hpp>
#include <istream>
#include <list>
#include <range/v3/all.hpp>

namespace loader {
namespace CollegeMsgLoader {
struct Entry
{
    uint64_t time{};
    int first{};
    int second{};
};

std::istream& operator>>(std::istream& istream, Entry& entry);

constexpr bool operator==(const Entry& lhs, const Entry& rhs);

constexpr bool operator!=(const Entry& lhs, const Entry& rhs)
{
    return !(lhs == rhs);
}

struct GroupByTimeMask
{
    std::string previousValue{};
    std::string mask = "%Y%m%d%H";
    GroupByTimeMask(std::string mask) : mask(mask) {}
    GroupByTimeMask(const GroupByTimeMask& other) = default;
    GroupByTimeMask(GroupByTimeMask&& other) = default;
    GroupByTimeMask& operator=(const GroupByTimeMask& other) = default;
    GroupByTimeMask& operator=(GroupByTimeMask&& other) = default;

    std::string applyTimeMask(uint64_t time)
    {
        char buffer[80];
        time_t tt = time;
        auto timeinfo = localtime(&tt);
        strftime(buffer, sizeof(buffer), mask.c_str(), timeinfo);
        return buffer;
    }

    bool operator()(Entry& entry) noexcept
    {
        auto value = applyTimeMask(entry.time);
        bool ret = previousValue.size() && previousValue != value;
        previousValue = std::move(value);
        return ret;
    }
};

inline Triplet entryToTriplet(const Entry& entry)
{
    return Triplet{entry.first, entry.second, true};
};

inline auto entryListToTriplets(std::list<Entry>& list)
{
    Triplets out{};
    out.reserve(ranges::distance(list));
    for (auto& entry : list) {
        auto triplet = entryToTriplet(entry);
        out.emplace_back(triplet);
    }
    return out;
};

inline auto getEntriesRangeGroupedByTimeMask(std::istream& input, std::string mask)
{
    return helpers::makeSplitInputRange(std::istream_iterator<Entry>{input}, GroupByTimeMask{mask});
}

inline auto getTripletsGroupedByTimeMask(std::istream& input, std::string mask)
{
    auto entriesList = getEntriesRangeGroupedByTimeMask(input, mask);
    return entriesList | ranges::view::transform(entryListToTriplets);
}

} // namespace CollegeMsgLoader

} // namespace loader

BOOST_FUSION_ADAPT_STRUCT(loader::CollegeMsgLoader::Entry, time, first, second);

constexpr bool loader::CollegeMsgLoader::operator==(const Entry& lhs, const Entry& rhs)
{
    using namespace boost::fusion;
    return equal_to(lhs, rhs);
}
