#pragma once
#include <boost/fusion/adapted/boost_tuple.hpp>
#include <boost/fusion/adapted/struct/adapt_struct.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/fusion/include/algorithm.hpp>
#include <boost/fusion/include/boost_tuple.hpp>
#include <boost/fusion/include/sequence.hpp>
#include <gsl/gsl_util>
#include <helpers/SplitInputIterator.hpp>
#include <helpers/Types.hpp>
#include <istream>
#include <list>
#include <range/v3/all.hpp>

namespace loader {
namespace SexContact {
enum class Grade : int
{
    Good = 1,
    Neutral = 0,
    Bad = -1
};
enum class FuzzyBool : int
{
    Yes = 1,
    NoInfo = 0,
    No = -1
};

struct Entry
{
    int femaleId{};
    int maleId{};
    int day{};
    Grade femaleGrade{};
    FuzzyBool contactOne{};
    FuzzyBool contactTwo{};
    FuzzyBool contactThree{};
};

std::istream& operator>>(std::istream& istream, Entry& entry);

constexpr bool operator==(const Entry& lhs, const Entry& rhs);

constexpr bool operator!=(const Entry& lhs, const Entry& rhs)
{
    return !(lhs == rhs);
}

inline auto getEntriesRange(std::istream& input)
{
    return ranges::iterator_range<std::istream_iterator<Entry>>(input, std::istream_iterator<Entry>{});
}

struct SplitByDay
{
    int previousValue = -1;
    constexpr SplitByDay() {}
    constexpr SplitByDay(const SplitByDay& other) = default;
    constexpr SplitByDay(SplitByDay&& other) = default;
    SplitByDay& operator=(const SplitByDay& other) = default;
    SplitByDay& operator=(SplitByDay&& other) = default;

    bool operator()(Entry& entry) noexcept
    {
        // auto finally = gsl::finally([this, &entry] { previousValue = entry.day; });
        bool ret = previousValue != -1 && previousValue != entry.day;
        previousValue = entry.day;
        return ret;
    }
};

inline Triplet entryToTriplet(const Entry& entry)
{
    // even ids are female, odd ids are male. They do not overlap due to this operation
    return Triplet{2 * entry.femaleId, 2 * entry.maleId + 1, true};
};

inline auto entryListToTriplets(std::list<Entry>& list)
{
    Triplets out{};
    out.reserve(ranges::distance(list));
    int day;
    for (auto& entry : list) {
        auto triplet = entryToTriplet(entry);
        out.emplace_back(triplet);
        day = list.front().day;
    }
    return std::make_pair(day, out);
};

inline auto getEntriesRangeByDay(std::istream& input)
{
    return helpers::makeSplitInputRange(std::istream_iterator<Entry>{input}, SplitByDay{});
}

inline auto getTripletsPerDay(std::istream& input)
{
    auto entriesListPerDay = getEntriesRangeByDay(input);
    auto tripletsListPerDay = entriesListPerDay | ranges::view::transform(entryListToTriplets);

    constexpr auto maxDays = 5030;
    const auto tripletsPerDay = [&] {
        std::vector<Triplets> tripletsPerDay(maxDays);
        int lastDay = 0;

        for (auto pair : tripletsListPerDay) {
            auto& [day, triplets] = pair;
            tripletsPerDay[day] = std::move(triplets);
            lastDay = day;

            if (day >= maxDays)
                break;
        }
        tripletsPerDay.resize(lastDay + 1);
        return std::move(tripletsPerDay);
    }();

    return tripletsPerDay;
}

} // namespace SexContact

} // namespace loader

BOOST_FUSION_ADAPT_STRUCT(loader::SexContact::Entry, femaleId, maleId, day, femaleGrade, contactOne, contactTwo,
                          contactThree);

constexpr bool loader::SexContact::operator==(const Entry& lhs, const Entry& rhs)
{
    using namespace boost::fusion;
    return equal_to(lhs, rhs);
}
