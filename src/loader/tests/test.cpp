#include <gtest/gtest.h>
#include <loader/SexContactsLoader.hpp>
#include <range/v3/all.hpp>
#include <sstream>

using namespace testing;

TEST(SexContactsLoader, TestEquality)
{
    using namespace loader::SexContact;
    Entry first{10, 10, 10, Grade::Bad, FuzzyBool::No, FuzzyBool::NoInfo, FuzzyBool::Yes};
    Entry second{10, 10, 10, Grade::Bad, FuzzyBool::No, FuzzyBool::NoInfo, FuzzyBool::Yes};
    Entry third{20, 10, 10, Grade::Bad, FuzzyBool::No, FuzzyBool::NoInfo, FuzzyBool::Yes};

    ASSERT_EQ(first, second);
    ASSERT_NE(first, third);
    ASSERT_NE(second, third);
}

namespace {
using namespace loader::SexContact;
std::string firstString = "10;20;30;1;1;0;-1";
constexpr auto firstEntry = Entry{10, 20, 30, Grade::Good, FuzzyBool::Yes, FuzzyBool::NoInfo, FuzzyBool::No};
std::string secondString = "30;40;50;-1;-1;0;1";
constexpr auto secondEntry = Entry{30, 40, 50, Grade::Bad, FuzzyBool::No, FuzzyBool::NoInfo, FuzzyBool::Yes};
} // namespace

TEST(SexContactsLoader, TestIstreamOperator)
{
    std::istringstream input(firstString);
    using namespace loader::SexContact;
    Entry entry;
    input >> entry;
    ASSERT_EQ(entry, firstEntry);
}

TEST(SexContactsLoader, TestIstreamIterator)
{
    std::istringstream input(firstString + "\n" + secondString);
    using namespace loader::SexContact;
    std::istream_iterator<Entry> reader(input);
    auto entry = *reader++;
    ASSERT_EQ(entry, firstEntry);
    entry = *reader++;
    ASSERT_EQ(entry, secondEntry);
}

TEST(SexContactsLoader, TestIstreamIteratorRange)
{
    std::istringstream input(firstString + '\n' + firstString + '\n' + secondString + '\n' + secondString);
    using namespace loader::SexContact;
    auto range = getEntriesRange(input);

    for (auto entry : range | ranges::view::take(2)) {
        ASSERT_EQ(entry, firstEntry);
    }

    for (auto entry : range | ranges::view::drop(2) | ranges::view::take(2)) {
        ASSERT_EQ(entry, secondEntry);
    }
}

TEST(SexContactsLoader, TestRangeTransform)
{
    std::istringstream input(firstString + '\n' + firstString + '\n' + secondString + '\n' + secondString);
    using namespace loader::SexContact;
    auto entries = getEntriesRange(input);
    auto tf = ranges::view::transform(entries, [](auto entry) {
        entry.day += 2;
        return entry;
    });

    for (auto entry : tf | ranges::view::take(2)) {
        ASSERT_EQ(entry.day, firstEntry.day + 2);
    }

    for (auto entry : tf | ranges::view::drop(2) | ranges::view::take(2)) {
        ASSERT_EQ(entry.day, secondEntry.day + 2);
    }
}

TEST(SexContactsLoader, TestDayRange)
{
    std::istringstream input(firstString + '\n' + firstString + '\n' + secondString + '\n' + secondString);
    auto range = loader::SexContact::getEntriesRangeByDay(input);

    int k = 0;
    for (auto dayList : range) {
        ASSERT_EQ(dayList.size(), 2);
        if (k++ == 0) {
            ASSERT_EQ(dayList.front(), firstEntry);
            ASSERT_EQ(dayList.back(), firstEntry);
        } else {
            ASSERT_EQ(dayList.front(), secondEntry);
            ASSERT_EQ(dayList.back(), secondEntry);
        }
    }
}
